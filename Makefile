
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)
suffix := $(shell echo $(VERSION) | grep -o '^[0-9]*\.[0-9]*')
bindir := $(prefix)/bin
libdir := $(prefix)/lib
datadir := $(prefix)/share/clockmaker$(suffix)
docdir := $(prefix)/share/doc/clockmaker$(suffix)
htmldir := $(docdir)
pdfdir := $(docdir)
localstatedir := $(prefix)/var/clockmaker

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# Overridable flags

CXXFLAGS ?= -g
CPPFLAGS ?=
LDLIBS ?=

# Override these flags so they can't be re-overriden.

override CXXFLAGS += -std=c++14
override CXXFLAGS += -MMD
override CXXFLAGS += -fPIC
#override CXXFLAGS += -Wall
#override CXXFLAGS += -Werror
#override CXXFLAGS += -Wfatal-errors

override CPPFLAGS += -I$(root_dir)

override LDLIBS += -lm
override LDLIBS += -lstdc++

# Compilation takes place in...

vpath %.cc $(root_dir)src

# Do not allow direct compilation of % from %.cc; it breaks -MMD

%: %.cc

.PHONY: all
all: clockmaker

.PHONY: clean
clean:
	$(RM) *.o
	$(RM) *.d
	$(RM) clockmaker

.PHONY: install
install: all
	$(INSTALL_PROGRAM) clockmaker $(DESTDIR)$(bindir)/clockmaker
	$(INSTALL) -d $(DESTDIR)$(datadir)/rooms
	$(INSTALL_DATA) $(root_dir)data/rooms/* $(DESTDIR)$(datadir)/rooms
	$(INSTALL) -d $(DESTDIR)$(datadir)/samples
	$(INSTALL_DATA) $(root_dir)data/samples/* $(DESTDIR)$(datadir)/samples
	$(INSTALL) -d $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/* $(DESTDIR)$(datadir)/textures
	$(INSTALL) -d $(DESTDIR)$(datadir)/lua
	$(INSTALL_DATA) $(root_dir)data/lua/* $(DESTDIR)$(datadir)/lua

.PHONY: uninstall
uninstall:
	$(RM) $(DESTDIR)$(bindir)/clockmaker
	$(RM) -r $(DESTDIR)$(datadir)/rooms
	$(RM) -r $(DESTDIR)$(datadir)/samples
	$(RM) -r $(DESTDIR)$(datadir)/textures
	$(RM) -r $(DESTDIR)$(datadir)/lua

clockmaker: CPPFLAGS += -DVERSION=\"$(VERSION)\"
clockmaker: CPPFLAGS += -DLIBDIR=\"$(libdir)\"
clockmaker: CPPFLAGS += -DDATADIR=\"$(datadir)\"
clockmaker: CPPFLAGS += -DLOCALSTATEDIR=\"$(localstatedir)\"
clockmaker: CPPFLAGS += -DLUABIND_USE_CXX11
clockmaker: CXXFLAGS += $(shell pkg-config --cflags lua52-c++ luabind sdl2 SDL2_image SDL2_mixer gl pugixml)
clockmaker: LDFLAGS += -pthread
clockmaker: LDLIBS += $(shell pkg-config --libs lua52-c++ luabind sdl2 SDL2_image SDL2_mixer gl pugixml)
clockmaker: LDLIBS += -lboost_serialization -lboost_filesystem -lboost_system
clockmaker: main_loop.o audio_effects.o physics.o quad_renderer.o tile_renderer.o tmx_parser.o world.o explore.o menu.o battle.o game.o clockmaker.o

-include $(shell find -name "*.d")

