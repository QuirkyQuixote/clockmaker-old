Clockmaker - A very retro RPG
=============================

Requirements
------------
Clockmaker requires:

- C++ compiler supporting the C++14 standard
- C++ boost libraries (serialization and filesystem)
- OpenGL
- SDL2 (core, image, and mixer)
- lua5.2
- luabind
- pugixml

Installation
------------
Clockmaker provides a simple Makefile; to build from scratch:

    make
    make install

Running 
-------

After installing, rin the clockmaker executable:

    clockmaker

After exiting the executable will create a snapshot of the game being played in
~/.clockmaker/savestate, and it will be restored from it in subsequent
executions. Delete it if you want to start from scratch.

Requires a gamepad.
