
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)
suffix := $(shell echo $(VERSION) | grep -o '^[0-9]*\.[0-9]*')
bindir := $(prefix)/bin
libdir := $(prefix)/lib
datadir := $(prefix)/share/clockmaker$(suffix)
docdir := $(prefix)/share/doc/clockmaker$(suffix)
htmldir := $(docdir)
pdfdir := $(docdir)
localstatedir := $(prefix)/var/clockmaker

# There are so many tools needed...

SH := sh
CXX := c++
AR := ar
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644
DIFF = diff

# These flags may be overriden by the user

CPPFLAGS :=
CXXFLAGS := -g -Werror -Wfatal-errors
LDFLAGS :=

# These flags are necessary

CXXFLAGS += -std=c++14
CXXFLAGS += -DVERSION=\"$(VERSION)\"
CXXFLAGS += -DLIBDIR=\"$(libdir)\"
CXXFLAGS += -DDATADIR=\"$(datadir)\"
CXXFLAGS += -DLOCALSTATEDIR=\"$(localstatedir)\"
CXXFLAGS += -MMD
CXXFLAGS += -I/usr/include/tcl

LDFLAGS += -lstdc++

# To go down a level $(call descend,directory[,target[,flags]])

descend = make -C $(1) $(2) $(3)

# If the -s option was passed, use these to give a hint of what we are doing

ifneq ($(findstring s,$(filter-out --%,$(MAKEFLAGS))),)
    QUIET_CC =		@echo "CC        $@";
    QUIET_CXX =		@echo "CXX       $@";
    QUIET_LINK =	@echo "LINK      $@";
    QUIET_AR =		@echo "AR        $@";
    QUIET_GEN =		@echo "GEN       $@";
    QUIET_UNZIP =	@echo "UNZIP     $@";
    QUIET_TEST =	@echo "TEST      $@";
    descend =		@echo "ENTER     `pwd`/$(1)";\
			 make -C $(1) $(2) $(3);\
			 echo "EXIT      `pwd`/$(1)"
endif


# Some generic targets that are the same for all Makefiles

%.h: gen_%.sh
	$(QUIET_GEN)sh $< > $@

.obj/%.o: %.cc | .obj
	$(QUIET_CXX)$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

.obj:
	mkdir .obj

%.a:
	$(QUIET_AR)$(AR) rc $@ $^

%: .obj/%.o
	$(QUIET_LINK)$(CXX) $(LDFLAGS) -o $@ $^ 

