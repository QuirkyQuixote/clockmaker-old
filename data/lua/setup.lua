
objects = objects or {}

function cm.init()
        for k,v in pairs(objects) do
                if v.init then
                        v:init()
                end
        end
end

function cm.update()
        for k,v in pairs(objects) do
                if v.update then
                        v:update()
                end
        end
end

function cm.render()
        for k,v in pairs(objects) do
                if v.render then
                        v:render()
                end
        end
end

function attack(c, d, skill, dmg1, dmg2)
        local atk = cm.roll(3)
        local dmg = 0
        if atk == 3 then
                dmg = dmg1 * 6 + dmg2
                cm.say(c.name.." crits "..d.name.." for "..dmg.." damage!")
                cm.audio.Source(AUDIO_DIR.."slash.wav", 0)
        elseif atk < c.cur.dx + skill then
                dmg = cm.roll(dmg1) + dmg2
                if dmg < 1 then dmg = 1 end
                cm.say(c.name.." hits "..d.name.." for "..dmg.." damage!")
                cm.audio.Source(AUDIO_DIR.."punch.wav", 0)
        else
                cm.say(c.name.." misses "..d.name.."!")
                cm.audio.Source(AUDIO_DIR.."fail.wav", 0)
        end
        d.cur.ht = d.cur.ht - dmg
end

punch = {
        action = function(c, d)
                attack(c, d, 4, 1, math.floor(c.cur.st / 2 - 7))
                c:wait(64)
        end
}

sword = {
        action = function(c, d)
                attack(c, d, 4, 2, math.floor(c.cur.st / 2 - 7))
                c:wait(64)
        end
}

gun = {
        action = function(c, d)
                if c:has('bullets') == "" then
                        cm.say(c.name.." is out of ammo!")
                        cm.audio.Source(AUDIO_DIR.."fail.wav", 0)
                        return
                end
                attack(c, d, 4, 2, 2)
                c:wait(64)
        end
}

aqvavit = {
        action = function(c, d)
                c:take("aqvavit")
                d.cur.ht = d.cur.ht + 5
                if d.cur.ht > d.max.ht then d.cur.ht = d.max.ht end
                cm.say("De~licious!")
                cm.audio.Source(AUDIO_DIR.."gulp.wav", 0);
                c:wait(64)
        end
}

medkit = {
        action = function(c, d)
                if cm.get_mode() == 2 then
                        cm.say("There's no time for that during battle!")
                elseif d.cur.ht == d.max.ht then
                        cm.say(d.name.." is in top health")
                else
                        local roll = cm.roll(3)
                        local hp = 0
                        if roll == 3 then
                                hp = 6
                        elseif roll <= c.cur.iq then
                                hp = cm.roll(1)
                        else
                                hp = 1
                        end
                        cm.say(d.name .. " recovers " .. hp .. " HT")
                        cm.audio.Source(AUDIO_DIR.."gulp.wav", 0);
                        d.cur.ht = d.cur.ht + hp
                        if d.cur.ht > d.max.ht then d.cur.ht = d.max.ht end
                        if cm.roll(1) == 6 then
                                cm.say("You need another medkit; this one is spent.")
                                c:take("medkit")
                        end
                end
        end
}
