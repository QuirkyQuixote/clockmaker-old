
#include "audio_effects.h"

#include <limits>
#include <algorithm>

#include "SDL2/SDL_mixer.h"

struct Mute_effect {
        void operator()(int chan, void* stream, int len)
        {
                memset(stream, 0, len);
        }
};

template<typename T> struct Gain_effect {
        Gain_effect(double coef) : coef{coef} {}
        void operator()(int chan, void* stream, int len)
        {
                T* first = (T*)stream;
                T* last = first + len / sizeof(*first);
                while (first != last) {
                        double y = *first * coef;
                        if (y < std::numeric_limits<T>::min())
                                y = std::numeric_limits<T>::min();
                        else if (y > std::numeric_limits<T>::max())
                                y = std::numeric_limits<T>::max();
                        *(first++) = y;
                }
        }
        double coef;
};

template<typename T, int N> class Low_pass_effect {
 public:
        Low_pass_effect(double coef) : coef{coef} {}
        void operator()(int chan, void* stream, int len)
        {
                T* first = (T*)stream;
                T* last = first + len / sizeof(*first);
                for (int i = 0; i < N; ++i) {
                        double y = carry[i] + coef * (first[0] - carry[i]);
                        if (y < std::numeric_limits<T>::min())
                                y = std::numeric_limits<T>::min();
                        else if (y > std::numeric_limits<T>::max())
                                y = std::numeric_limits<T>::max();
                        *(first++) = y;
                }
                while (first != last) {
                        double y = first[-N] + coef * (first[0] - first[-N]);
                        if (y < std::numeric_limits<T>::min())
                                y = std::numeric_limits<T>::min();
                        else if (y > std::numeric_limits<T>::max())
                                y = std::numeric_limits<T>::max();
                        *(first++) = y;
                }
                for (int i = 0; i < N; ++i)
                        carry[i] = last[i - N];
        }
        double coef;

 private:
        T carry[N];
};

template<typename T, int N> class Room_effect {
 public:
        Room_effect(const Box2f& room, const Vec2f* list)
                : room{room}, list{list} {}
        void operator()(int chan, void* data, int len)
        {
                Vec2f d{0, 0};
                d.x = std::max(d.x, room.min.x - list->x);
                d.x = std::max(d.x, list->x - room.max.x);
                d.y = std::max(d.y, room.min.y - list->y);
                d.y = std::max(d.y, list->y - room.max.y);
                auto e = size(d);
                if (e == 0) {
                        return;
                }
                if (e > 20) {
                        memset(data, 0, len);
                        return;
                }
                gain_effect.coef = 1. - e / 20.;
                gain_effect(chan, data, len);
                low_pass_effect.coef = 1 / (e + 1);
                low_pass_effect(chan, data, len);
        }

 private:
        Box2f room;
        const Vec2f* list;
        Gain_effect<T> gain_effect{0};
        Low_pass_effect<T, N> low_pass_effect{0};
};


template<typename T> struct Filter_wrapper {
        template<typename... Args> static int attach(int chan, Args... args)
        {
                return Mix_RegisterEffect(chan, func, done,
                                new T(std::forward<Args>(args)...));
        }
        static int detach(int chan)
        {
                return Mix_UnregisterEffect(chan, func);
        }
        static void func(int chan, void* data, int len, void *udata)
        {
                (*reinterpret_cast<T*>(udata))(chan, data, len);
        }
        static void done(int chan, void* udata)
        {
                delete reinterpret_cast<T*>(udata);
        }
};

template<typename T, typename... Args> int attach_effect(int chan, Args... args)
{
        return Filter_wrapper<T>::attach(chan, std::forward<Args>(args)...);
}

template<typename T> int detach_effect(int chan)
{
        return Filter_wrapper<T>::detach(chan);
}


int mute_effect(int chan, int enable)
{
        if (enable)
                return attach_effect<Mute_effect>(chan);
        else
                return detach_effect<Mute_effect>(chan);
}

int gain_effect(int chan, double coef)
{
        int channels;
        Uint16 format;
        Mix_QuerySpec(NULL, &format, &channels);
        int bits = (format & 0xFF);

        switch (bits) {
         case 8:
                if (coef)
                        return attach_effect<Gain_effect<Sint8>>(chan, coef);
                else
                        return detach_effect<Gain_effect<Sint8>>(chan);
         case 16:
                if (coef)
                        return attach_effect<Gain_effect<Sint16>>(chan, coef);
                else
                        return detach_effect<Gain_effect<Sint16>>(chan);
         case 32:
                if (coef)
                        return attach_effect<Gain_effect<Sint32>>(chan, coef);
                else
                        return detach_effect<Gain_effect<Sint32>>(chan);
         default:
                Mix_SetError("Unsupported audio format");
                return 0;
        }
}

int low_pass_effect(int chan, double coef)
{
        int channels;
        Uint16 format;
        Mix_QuerySpec(NULL, &format, &channels);
        int bits = (format & 0xFF);

        if (channels == 1) {
                switch (bits) {
                 case 8:
                        if (coef)
                                return attach_effect<Low_pass_effect<Sint8, 1>>(chan, coef);
                        else
                                return detach_effect<Low_pass_effect<Sint8, 1>>(chan);
                 case 16:
                        if (coef)
                                return attach_effect<Low_pass_effect<Sint16, 1>>(chan, coef);
                        else
                                return detach_effect<Low_pass_effect<Sint16, 1>>(chan);
                 case 32:
                        if (coef)
                                return attach_effect<Low_pass_effect<Sint32, 1>>(chan, coef);
                        else
                                return detach_effect<Low_pass_effect<Sint32, 1>>(chan);
                 default:
                        Mix_SetError("Unsupported audio format");
                        return 0;
                }
        } else if (channels == 2) {
                switch (bits) {
                 case 8:
                        if (coef)
                                return attach_effect<Low_pass_effect<Sint8, 2>>(chan, coef);
                        else
                                return detach_effect<Low_pass_effect<Sint8, 2>>(chan);
                 case 16:
                        if (coef)
                                return attach_effect<Low_pass_effect<Sint16, 2>>(chan, coef);
                        else
                                return detach_effect<Low_pass_effect<Sint16, 2>>(chan);
                 case 32:
                        if (coef)
                                return attach_effect<Low_pass_effect<Sint32, 2>>(chan, coef);
                        else
                                return detach_effect<Low_pass_effect<Sint32, 2>>(chan);
                 default:
                        Mix_SetError("Unsupported audio format");
                        return 0;
                }
        } else {
                Mix_SetError("Unsupported channel number");
                return 0;
        }
}

int room_effect(int chan, const Box2f& room, const Vec2f* listener)
{
        int channels;
        Uint16 format;
        Mix_QuerySpec(NULL, &format, &channels);
        int bits = (format & 0xFF);

        if (channels == 1) {
                switch (bits) {
                 case 8:
                        return attach_effect<Room_effect<Sint8, 1>>(chan, room, listener);
                 case 16:
                        return attach_effect<Room_effect<Sint16, 1>>(chan, room, listener);
                 case 32:
                        return attach_effect<Room_effect<Sint32, 1>>(chan, room, listener);
                 default:
                        Mix_SetError("Unsupported audio format");
                        return 0;
                }
        } else if (channels == 2) {
                switch (bits) {
                 case 8:
                        return attach_effect<Room_effect<Sint8, 2>>(chan, room, listener);
                 case 16:
                        return attach_effect<Room_effect<Sint16, 2>>(chan, room, listener);
                 case 32:
                        return attach_effect<Room_effect<Sint32, 2>>(chan, room, listener);
                 default:
                        Mix_SetError("Unsupported audio format");
                        return 0;
                }
        } else {
                Mix_SetError("Unsupported channel number");
                return 0;
        }
}

