#ifndef AUDIO_EFFECTS_H_
#define AUDIO_EFFECTS_H_

#include "box.h"

int mute_effect(int channel, int enable);
int gain_effect(int channel, double coef);
int low_pass_effect(int channel, double coef);
int room_effect(int channel, const Box2f& room, const Vec2f* listener);

#endif          // AUDIO_EFFECTS_H_
