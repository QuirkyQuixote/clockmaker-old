
#include "battle.h"

#include "game.h"

class Battle::Impl {
 public:
        Impl(World& world) : world{world} {}
        void setup()
        {
                music = Mix_PlayChannel(-1, Chunk(AUDIO_DIR "battle.ogg"), -1);
                for (auto& x : world.party)
                        x.recovery = 92;
                for (auto& x : world.enemy)
                        x.recovery = 92;
        }
        void setdown()
        {
                Mix_HaltChannel(music);
        }
        void render(Main_loop& ml)
        {
                Vec2f view{ml.window_size()};
                glClearColor(0, 0, 0, 1);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        void update(Main_loop& ml)
        {
                try {
                        world.eval(std::string{"objects.selection.selection:battle()"});
                } catch (std::exception& ex) {
                        world.messages.push_back(ex.what());
                } catch (...) {
                        world.messages.push_back("unspecified error");
                }
                if (std::find_if(world.enemy.begin(), world.enemy.end(),
                                        [](auto& ch) {return !ch.flags.knocked_out;})
                                == world.enemy.end())
                        Mix_FadeOutChannel(music, 3000);
                if (std::find_if(world.party.begin(), world.party.end(),
                                        [](auto& ch) {return !ch.flags.knocked_out;})
                                == world.party.end())
                        Mix_FadeOutChannel(music, 3000);
                if (!Mix_Playing(music)) {
                        world.enemy.clear();
                        world.mode = Mode::explore;
                }
        }
        void event(Main_loop& ml, Button button, bool state)
        {
        }

 private:
        World& world;
        int music;
};

Battle::Battle(World& world)
        : impl{std::make_unique<Battle::Impl>(world)}
{}
Battle::~Battle() = default;
void Battle::setup()
{
        impl->setup();
}
void Battle::setdown()
{
        impl->setdown();
}
void Battle::update(Main_loop& ml)
{
        impl->update(ml);
}
void Battle::render(Main_loop& ml)
{
        impl->render(ml);
}
void Battle::event(Main_loop& ml, Button button, bool state)
{
        impl->event(ml, button, state);
}
