
#ifndef BATTLE_H_
#define BATTLE_H_

#include "main_loop.h"
#include "world.h"

class Battle : public Noncopyable {
 public:
        Battle(World& world);
        ~Battle();
        void setup();
        void setdown();
        void render(Main_loop& ml);
        void update(Main_loop& ml);
        void event(Main_loop& ml, Button button, bool state);

 private:
        class Impl;
        std::unique_ptr<Impl> impl;
};

#endif          // BATTLE_H_
