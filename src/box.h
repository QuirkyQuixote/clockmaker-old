/* Copyright 017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_BOX_H_
#define SRC_BOX_H_

#include <algorithm>

#include "vec.h"

template<int S, typename T> struct Box {
        Box() = default;
        Box(const Vec<S, T>& min, const Vec<S, T>& max)
              : min{min}, max{max} {}
        template<typename U> Box(const Box<S, U>& other)
              : min(other.min), max(other.max) {}
        template<typename U> Box& operator=(const Box<S, U>& other)
        {
                min = other.min;
                max = other.max;
                return *this;
        }
        Box& operator=(T other)
        {
                min = other;
                max = other;
                return *this;
        }

        Box& operator+=(const Vec<S, T>& b)
        {
                min += b;
                max += b;
                return *this;
        }
        Box& operator+=(T other)
        {
                min += other;
                max += other;
                return *this;
        }

        Box& operator-=(const Vec<S, T>& b)
        {
                min -= b;
                max -= b;
                return *this;
        }
        Box& operator-=(T other)
        {
                min -= other;
                max -= other;
                return *this;
        }

        Box& operator*=(const Vec<S, T>& b)
        {
                min *= b;
                max *= b;
                return *this;
        }
        Box& operator*=(T other)
        {
                min *= other;
                max *= other;
                return *this;
        }

        Box& operator/=(const Vec<S, T>& b)
        {
                min /= b;
                max /= b;
                return *this;
        }
        Box& operator/=(T other)
        {
                min /= other;
                max /= other;
                return *this;
        }

        Box& operator<<=(const Vec<S, T>& b)
        {
                min <<= b;
                max <<= b;
                return *this;
        }
        Box& operator<<=(T other)
        {
                min <<= other;
                max <<= other;
                return *this;
        }

        Box& operator>>=(const Vec<S, T>& b)
        {
                min >>= b;
                max >>= b;
                return *this;
        }
        Box& operator>>=(T other)
        {
                min >>= other;
                max >>= other;
                return *this;
        }

        Box& operator&=(const Vec<S, T>& b)
        {
                min &= b;
                max &= b;
                return *this;
        }
        Box& operator&=(T other)
        {
                min &= other;
                max &= other;
                return *this;
        }

        Box& operator|=(const Vec<S, T>& b)
        {
                min |= b;
                max |= b;
                return *this;
        }
        Box& operator|=(T other)
        {
                min |= other;
                max |= other;
                return *this;
        }

        Vec<S, T> min;
        Vec<S, T> max;
};

template<int S, typename T> bool operator==(const Box<S, T>& a, const Box<S, T>& b)
{
        return a.min == b.min && a.max == b.max;
}
template<int S, typename T> bool operator!=(const Box<S, T>& a, const Box<S, T>& b)
{
        return a.min != b.min || a.max != b.max;
}

template<int S, typename A, typename B> auto operator+(const Box<S, A>& a, B b)
{
        return Box<S, decltype(*a.min.v + b)>{a.min + b, a.max + b};
}
template<int S, typename A, typename B> auto operator+(A a, const Box<S, B>& b)
{
        return Box<S, decltype(a + *b.min.v)>{a + b.min, a + b.max};
}
template<int S, typename A, typename B> auto operator+(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v + *b.v)>{a.min + b, a.max + b};
}

template<int S, typename A, typename B> auto operator-(const Box<S, A>& a, B b)
{
        return Box<S, decltype(*a.min.v - b)>{a.min - b, a.max - b};
}
template<int S, typename A, typename B> auto operator-(A a, const Box<S, B>& b)
{
        return Box<S, decltype(a - *b.min.v)>{a - b.min, a - b.max};
}
template<int S, typename A, typename B> auto operator-(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v - *b.v)>{a.min - b, a.max - b};
}

template<int S, typename A, typename B> auto operator*(const Box<S, A>& a, B b)
{
        return Box<S, decltype(*a.min.v * b)>{a.min * b, a.max * b};
}
template<int S, typename A, typename B> auto operator*(A a, const Box<S, B>& b)
{
        return Box<S, decltype(a * *b.min.v)>{a * b.min, a * b.max};
}
template<int S, typename A, typename B> auto operator*(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v * *b.v)>{a.min * b, a.max * b};
}

template<int S, typename A, typename B> auto operator/(const Box<S, A>& a, B b)
{
        return Box<S, decltype(*a.min.v / b)>{a.min / b, a.max / b};
}
template<int S, typename A, typename B> auto operator/(A a, const Box<S, B>& b)
{
        return Box<S, decltype(a / *b.min.v)>{a / b.min, a / b.max};
}
template<int S, typename A, typename B> auto operator/(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v / *b.v)>{a.min / b, a.max / b};
}

template<int S, typename A> auto operator<<(const Box<S, A>& a, long b)
{
        return Box<S, decltype(*a.min.v << b)>{a.min << b, a.max << b};
}
template<int S, typename B> auto operator<<(long a, const Box<S, B>& b)
{
        return Box<S, decltype(a << *b.min.v)>{a << b.min, a << b.max};
}
template<int S, typename A, typename B> auto operator<<(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v << *b.v)>{a.min << b, a.max << b};
}

template<int S, typename A> auto operator>>(const Box<S, A>& a, long b)
{
        return Box<S, decltype(*a.min.v >> b)>{a.min >> b, a.max >> b};
}
template<int S, typename B> auto operator>>(long a, const Box<S, B>& b)
{
        return Box<S, decltype(a >> *b.min.v)>{a >> b.min, a >> b.max};
}
template<int S, typename A, typename B> auto operator>>(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v >> *b.v)>{a.min >> b, a.max >> b};
}

template<int S, typename A> auto operator&(const Box<S, A>& a, long b)
{
        return Box<S, decltype(*a.min.v & b)>{a.min & b, a.max & b};
}
template<int S, typename B> auto operator&(long a, const Box<S, B>& b)
{
        return Box<S, decltype(a & *b.min.v)>{a & b.min, a & b.max};
}
template<int S, typename A, typename B> auto operator&(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v & *b.v)>{a.min & b, a.max & b};
}

template<int S, typename A> auto operator|(const Box<S, A>& a, long b)
{
        return Box<S, decltype(*a.min.v | b)>{a.min | b, a.max | b};
}
template<int S, typename B> auto operator|(long a, const Box<S, B>& b)
{
        return Box<S, decltype(a | *b.min.v)>{a | b.min, a | b.max};
}
template<int S, typename A, typename B> auto operator|(const Box<S, A>& a, const Vec<S, B>& b)
{
        return Box<S, decltype(*a.min.v | *b.v)>{a.min | b, a.max | b};
}

template<int S, typename T> Box<S, T> floor(const Box<S, T>& v)
{
        return Box<S, T>{floor(v.min), floor(v.max)};
}

template<int S, typename T> Box<S, T> ceil(const Box<S, T>& v)
{
        return Box<S, T>{ceil(v.min), ceil(v.max)};
}

template<int S, typename T> Box<S, T> pow(const Box<S, T>& v)
{
        return Box<S, T>{pow(v.min), pow(v.max)};
}

template<int S, typename T> Vec<S, T> size(const Box<S, T>& a)
{
        return a.max - a.min;
}
template<int S, typename T> Vec<S, T> center(const Box<S, T>& a)
{
        return (a.max + a.min) / 2;
}
template<int S, typename T> Box<S, T> fix(const Box<S, T>& a)
{
        for (int i = 0; i < S; ++i)
                if (a.min.v[i] > a.max.v[i])
                        std::swap(a.min.v[i], a.max.v[i]);
        return a;
}
template<int S, typename A, typename B> bool overlap(const Box<S, A>& a, const Box<S, B>& b)
{
        for (int i = 0; i < S; ++i)
                if (b.min.v[i] > a.max.v[i] || b.max.v[i] < a.min.v[i])
                        return false;
        return true;
}
template<int S, typename A, typename B> bool contains(const Box<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
                if (b.v[i] < a.min.v[i] || b.v[i] > a.max.v[i])
                        return false;
        return true;
}
template<int S, typename T> Box<S, T> flip(const Box<S, T>& a, int axis)
{
        auto b{a};
        std::swap(b.min.v[axis], b.max.v[axis]);
        return b;
}
template<int S, typename T> Box<S, T> shift(const Box<S, T>& a, const Vec<S, T>& b)
{
        return Box<S, T>{a.min + b, a.max + b};
}
template<int S, typename T> Box<S, T> grow(const Box<S, T>& a, T b)
{
        return Box<S, T>{a.min - b, a.max + b};
}
template<int S, typename T> Box<S, T> grow(const Box<S, T>& a, const Vec<S, T>& b)
{
        return Box<S, T>{a.min - b, a.max + b};
}
template<int S, typename T> Box<S, T> shrink(const Box<S, T>& a, T b)
{
        return Box<S, T>{a.min + b, a.max - b};
}
template<int S, typename T> Box<S, T> shrink(const Box<S, T>& a, const Vec<S, T>& b)
{
        return Box<S, T>{a.min + b, a.max - b};
}
template<int S, typename T> std::ostream& operator<<(std::ostream& s, const Box<S, T>& b)
{
        return s << '(' << b.min << ' ' << b.max << ')';
}

template<int S, typename T> struct Box_iterator {
        const Box<S, T> &base;
        Vec<S, T> cur;

        Box_iterator<S, T>& operator++()
        {
                for (int i = 0; i < S; ++i) {
                        ++cur.v[i];
                        if (cur.v[i] != base.max.v[i] || i == S - 1)
                                break;
                        else
                                cur.v[i] = base.min.v[i];
                }
                return *this;
        }
        bool operator==(const Box_iterator<S, T>& other) const
        {
                return cur == other.cur;
        }
        bool operator!=(const Box_iterator<S, T>& other) const
        {
                return cur != other.cur;
        }
        const Vec<S, T>& operator*() const { return cur; }
        const Vec<S, T>* operator->() const { return &cur; }
};

template<int S, typename T> Box_iterator<S, T> begin(const Box<S, T>& box)
{
        return Box_iterator<S, T>{box, box.min};
}
template<int S, typename T> Box_iterator<S, T> end(const Box<S, T>& box)
{
        Vec<S, T> r{box.min};
        r.v[S - 1] = box.max.v[S - 1];
        return Box_iterator<S, T>{box, r};
}

typedef Box<2, int> Box2i;
typedef Box<2, float> Box2f;
typedef Box<3, int> Box3i;
typedef Box<3, float> Box3f;

#endif          // SRC_BOX_H_
