/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_CHARACTER_H_
#define SRC_CHARACTER_H_

#include <stdlib.h>
#include <string>
#include <array>
#include <algorithm>
#include <random>
#include <regex>

struct Character_attrs {
        char st{10};
        char dx{10};
        char iq{10};
        char ht{10};
        char hp{10};
        char will{10};
        char per{10};
        char fp{10};
};

enum class Posture {
        stand,
        crouch,
        kneel,
        crawl,
        sit,
        lie,
};

struct Character_flags {
        bool knocked_out{false};
        bool ready{false};
};

struct Character {
        std::string name;
        Character_attrs cur;
        Character_attrs max;
        Character_flags flags;
        Posture posture;
        std::array<std::string, 8> inventory{{"punch"}};
        int recovery{0};

        bool give(const std::string& item)
        {
                for (auto& x : inventory)
                        if (x == "") {
                                x = item;
                                return true;
                        }
                return false;
        }
        bool take(const std::string& item)
        {
                for (auto& x : inventory)
                        if (x == item) {
                                x = "";
                                return true;
                        }
                return false;
        }
        std::string has(const std::string& item)
        {
                std::regex r{item};
                for (auto& x : inventory)
                        if (std::regex_match(x, r))
                                return x;
                return "";
        }
        void wait(int ticks)
        {
                recovery += ticks;
        }
};

#endif          // SRC_CHARACTER_H_
