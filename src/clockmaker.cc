
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"

#include "game.h"
#include "tmx_parser.h"
#include "serialize.h"
#include "slurp.h"

#define ROOM_DIR DATADIR "/rooms/"

int main(int argc, char *argv[])
{
        Main_loop ml;
        Game game{world()};

        world().eval(boost::filesystem::path{LUA_DIR "setup.lua"});

        boost::filesystem::path cwd;
        cwd /= getenv("HOME");
        cwd /= ".clockmaker";
        boost::filesystem::create_directories(cwd);
        boost::filesystem::current_path(cwd);

        load(world(), ROOM_DIR "nuskom.tmx");

        if (std::ifstream ifs{"savestate"})
                boost::archive::binary_iarchive{ifs} >> world();
        else
                std::cerr << "savestate: can't open\n";

        ml.run(game);

        if (std::ofstream ofs{"savestate"})
                boost::archive::binary_oarchive{ofs} << world();
        else
                std::cerr << "savestate: can't open\n";

        return 0;
}

