/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_COMMAND_LINE_H_
#define SRC_COMMAND_LINE_H_

#include <string>
#include <vector>

class Command_line {
 public:
        const std::string& buf() const { return current; }
        int cur() const { return cursor; }

        bool new_buffer()
        {
                buffer = "";
                current = buffer;
                focus = history.size();
                cursor = 0;
                return true;
        }
        bool backward_char()
        {
                if (cursor == 0)
                        return false;
                --cursor;
                return true;
        }
        bool forward_char()
        {
                if (current[cursor] == 0)
                        return -false;
                ++cursor;
                return true;
        }
        bool backward_word()
        {
                if (cursor == 0)
                        return false;
                --cursor;
                while (cursor >= 0 && !isalnum(current[cursor]))
                        --cursor;
                while (cursor >= 0 && isalnum(current[cursor]))
                        --cursor;
                ++cursor;
                return true;
        }
        bool forward_word()
        {
                if (current[cursor + 1] == 0)
                        return false;
                while (current[cursor] != 0 && !isalnum(current[cursor]))
                        ++cursor;
                while (current[cursor] != 0 && isalnum(current[cursor]))
                        ++cursor;
                while (current[cursor] != 0 && !isalnum(current[cursor]))
                        ++cursor;
                return true;
        }
        bool beginning_of_line()
        {
                cursor = 0;
                return true;
        }
        bool end_of_line()
        {
                cursor = current.size();
                return true;
        }
        bool previous_history()
        {
                if (focus == 0)
                        return false;
                --focus;
                current = history[focus];
                cursor = current.size();
                return true;
        }
        bool next_history()
        {
                if (focus == history.size())
                        return false;
                ++focus;
                if (focus == history.size())
                        current = buffer;
                else
                        current = history[focus];
                cursor = current.size();
                return true;
        }
        bool beginning_of_history()
        {
                focus = 0;
                current = history[focus];
                cursor = current.size();
                return true;
        }
        bool end_of_history()
        {
                focus = history.size();
                current = buffer;
                cursor = current.size();
                return true;
        }
        bool end_of_file()
        {
                if (current.empty())
                        return true;
                return delete_char();
        }
        bool insert_str(const char *str, size_t len)
        {
                pop_line();
                buffer.insert(cursor, str, len);
                current = buffer;
                cursor += len;
                return true;
        }
        bool insert_char(int c)
        {
                pop_line();
                buffer.insert(cursor, 1, c);
                current = buffer;
                ++cursor;
                return true;
        }
        bool delete_char()
        {
                if (current[cursor] == 0)
                        return false;
                pop_line();
                buffer.erase(cursor, 1);
                current = buffer;
                return true;
        }
        bool backward_delete_char()
        {
                return backward_char() && delete_char();
        }
        bool yank()
        {/*
                if (clipboard.size())
                        insert_str(clipboard.str, clipboard.len);
                        */
                return true;
        }
        bool accept_line()
        {
                pop_line();
                push_line();
                return true;
        }

 private:
        std::string buffer;
        std::string clipboard;
        std::vector<std::string> history;
        std::string current{buffer};
        int focus;
        int cursor;

        bool pop_line()
        {
                if (current != buffer) {
                        buffer = current;
                        current = buffer;
                        focus = history.size();
                        return true;
                }
                return false;
        }
        bool push_line()
        {
                if (history.empty() || current != history.back())
                        history.push_back(current);
                return true;
        }
};

#endif          // SRC_COMMAND_LINE_H_
