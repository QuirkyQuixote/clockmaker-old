
#ifndef _SRC_DICT_H
#define _SRC_DICT_H

#include <algorithm>
#include <functional>
#include <vector>

template<typename Key,
        typename T,
        class Base = std::vector<std::pair<Key, T>>,
        class Compare = std::less<Key>
        >
class Dict {
 public:
        typedef Key key_type;
        typedef T mapped_type;
        typedef typename Base::value_type value_type;
        typedef typename Base::reference reference;
        typedef typename Base::const_reference const_reference;
        typedef typename Base::pointer pointer;
        typedef typename Base::const_pointer const_pointer;
        typedef typename Base::iterator iterator;
        typedef typename Base::const_iterator const_iterator;

        T& at(const key_type& k)
        {
                auto it = std::lower_bound(base_.begin(), base_.end(), k,
                        [this](auto& x, auto& y){ return comp_(x.first, y); });
                if (it == base_.end() || comp_(k, it->first))
                        throw std::out_of_range{"no such key in dictionary"};
                return it->second;
        }
        const T& at(const key_type& k) const
        {
                auto it = std::lower_bound(base_.begin(), base_.end(), k,
                        [this](auto& x, auto& y){ return comp_(x.first, y); });
                if (it == base_.end() || comp_(k, it->first))
                        throw std::out_of_range{"no such key in dictionary"};
                return it->second;
        }
        T& operator[](const key_type& k)
        {
                auto it = std::lower_bound(base_.begin(), base_.end(), k,
                        [this](auto& x, auto& y){ return comp_(x.first, y); });
                if (it == base_.end() || comp_(k, it->first))
                        it = base_.emplace(it, k, T{});
                return it->second;
        }
        T& operator[](key_type&& k)
        {
                auto it = std::lower_bound(base_.begin(), base_.end(), k,
                        [this](auto& x, auto& y){ return comp_(x.first, y); });
                if (it == base_.end() || comp_(k, it->first))
                        it = base_.emplace(it, std::forward<std::string>(k), T{});
                return it->second;
        }

        iterator begin() { return base_.begin(); }
        const_iterator begin() const { return base_.begin(); }
        const_iterator cbegin() const { return base_.cbegin(); }
        iterator end() { return base_.end(); }
        const_iterator end() const { return base_.end(); }
        const_iterator cend() const { return base_.cend(); }
        iterator rbegin() { return base_.rbegin(); }
        const_iterator rbegin() const { return base_.rbegin(); }
        const_iterator crbegin() const { return base_.crbegin(); }
        iterator rend() { return base_.rend(); }
        const_iterator rend() const { return base_.rend(); }
        const_iterator crend() const { return base_.crend(); }

        Base& base() { return base_; }
        const Base& base() const { return base_; }

 private:
        Base base_;
        Compare comp_;
};

#endif          // _SRC_DICT_H
