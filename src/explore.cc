

#include "explore.h"

#include "boost/filesystem.hpp"

#include "sky_renderer.h"
#include "tile_renderer.h"

class Explore::Impl {
 public:
        Impl(World& world) : world{world} {}
        void setup()
        {
                for (auto c : world.audio_channels)
                        Mix_Resume(c);
        }
        void setdown()
        {
                for (auto c : world.audio_channels)
                        Mix_Pause(c);
        }
        void update(Main_loop& ml)
        {
                if (just_loaded) {
                        just_loaded = false;
                        std::cout << "Pre render background..." << std::endl;
                        back_renderer.update(world.back, Box2f{Vec2i{}, world.back.dimensions()});
                        std::cout << "Pre render foreground..." << std::endl;
                        front_renderer.update(world.front, Box2f{Vec2i{}, world.front.dimensions()});
                        std::cout << "Done!" << std::endl;
                        run_script("cm.init()");
                } else {
                        run_script("cm.update()");
                }
                world.space.step(1.f / ml.get_fps());
                update_audio();
        }
        void render(Main_loop& ml)
        {
                Vec2f view{ml.window_size()};
                Box2f clip = place_camera(view);
                glClearColor(0, 0, 0, 1);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                glColor3f(1, 1, 1);
                if (layers & (int)Layer::sky) {
                        Vec2f c = (clip.max + clip.min) * .5f;
                        Vec2f s = view * .5f / 24;
                        sky_renderer.render(Box2f{c - s, c + s});
                }
                if (layers & (int)Layer::back)
                        back_renderer.render(clip);
                if (layers & (int)Layer::objects)
                        run_script("cm.render()");
                if (layers & (int)Layer::front)
                        front_renderer.render(clip);
        }
        void event(Main_loop& ml, Button button, bool state)
        {
                std::stringstream buf;
                buf << "objects.player:button(\"" << button << "\"," <<
                        (state ? "true" : "false") << ")";
                run_script(buf.str());
        }

 private:
        enum class Layer {
                sky = 1 << 0,
                back = 1 << 1,
                front = 1 << 2,
                objects = 1 << 3,
        };

        World& world;
        bool just_loaded{true};
        Sky_renderer sky_renderer;
        Tile_renderer back_renderer{Texture{TEXTURE_DIR "nuskom.gif"}};
        Tile_renderer front_renderer{Texture{TEXTURE_DIR "nuskom.gif"}};
        int layers = 0xFF;

        void run_script(const std::string& cmd)
        {
                try {
                        world.eval(cmd);
                } catch (std::exception& ex) {
                        std::cerr << ex.what() << "\n";
                } catch (...) {
                        std::cerr << "unrecognized error\n";
                }
        }
        void update_audio()
        {
                auto x = std::remove_if(world.audio_channels.begin(),
                                world.audio_channels.end(),
                                [](int chan){ return !Mix_Playing(chan); });
                world.audio_channels.erase(x, world.audio_channels.end());
        }
        Box2f place_camera(Vec2f view_size)
        {
                static const float scale = 24.f;
                Box2f clip{view_size * -.5f, view_size * .5f};
                clip /= scale;
                Vec2f off = world.camera;
                clip += floor(off * scale) / scale;
                Box2f view{Vec2f{}, view_size};
                glViewport(view.min.x, view.min.y, view.max.x - view.min.x, view.max.y - view.min.y);
                glDisable(GL_DEPTH_TEST);
                glDisable(GL_CULL_FACE);
                glMatrixMode(GL_PROJECTION);
                glLoadIdentity();
                glOrtho(clip.min.x, clip.max.x, clip.max.y, clip.min.y, 0, 1);
                return clip;
        }
};

Explore::Explore(World& world)
        : impl{std::make_unique<Explore::Impl>(world)}
{}
Explore::~Explore() = default;
void Explore::update(Main_loop& ml)
{
        impl->update(ml);
}
void Explore::setup()
{
        impl->setup();
}
void Explore::setdown()
{
        impl->setdown();
}
void Explore::render(Main_loop& ml)
{
        impl->render(ml);
}
void Explore::event(Main_loop& ml, Button button, bool state)
{
        impl->event(ml, button, state);
}
