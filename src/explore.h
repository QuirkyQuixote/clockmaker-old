/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_EXPLORE_H_
#define SRC_EXPLORE_H_

#include "main_loop.h"
#include "world.h"

class Explore : public Noncopyable {
 public:
        Explore(World& world);
        ~Explore();
        void setup();
        void setdown();
        void update(Main_loop& ml);
        void render(Main_loop& ml);
        void event(Main_loop& ml, Button button, bool state);

 private:
        class Impl;
        std::unique_ptr<Impl> impl;
};

#endif          // SRC_EXPLORE_H_
