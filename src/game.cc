/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#include "game.h"

#include <memory>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>

#include "explore.h"
#include "menu.h"
#include "battle.h"
#include "quad_renderer.h"

int line_width(const std::string& s)
{
        int l = 0, max = 0;
        for (auto c : s) {
                if (c == '\n') {
                        max = std::max(l, max);
                        l = 0;
                } else {
                        ++l;
                }
        }
        return std::max(l, max);
}

class Game::Impl {
 public:
        Impl(World& world)
                : world{world}, explore{world}, menu{world}, battle{world} {}
        void render(Main_loop& ml)
        {
                switch (mode) {
                 case Mode::explore:
                        explore.render(ml);
                        break;
                 case Mode::manage:
                        explore.render(ml);
                        menu.render(ml);
                        break;
                 case Mode::battle:
                        battle.render(ml);
                        menu.render(ml);
                        break;
                }
                render_messages(ml);
                render_command_line();
        }
        void update(Main_loop& ml)
        {
                if (mode != world.mode)
                        set_mode(world.mode);
                if (world.locked)
                        return;
                switch (mode) {
                 case Mode::explore:
                        explore.update(ml);
                        break;
                 case Mode::manage:
                        menu.update(ml);
                        break;
                 case Mode::battle:
                        battle.update(ml);
                        menu.update(ml);
                        break;
                }
        }
        void event(Main_loop& ml, const SDL_Event &e)
        {
                if (e.type == SDL_JOYBUTTONDOWN) {
                        if (e.cbutton.button == 0) {
                                propagate_event(ml, Button::action, true);
                        } else if (e.cbutton.button == 1) {
                                propagate_event(ml, Button::cancel, true);
                        } else if (e.cbutton.button == 2) {
                                if (mode == Mode::explore)
                                        world.mode = Mode::manage;
                                else if (mode == Mode::manage)
                                        world.mode = Mode::explore;
                        }
                } else if (e.type == SDL_JOYBUTTONUP) {
                        if (e.cbutton.button == 0)
                                propagate_event(ml, Button::action, false);
                        else if (e.cbutton.button == 1)
                                propagate_event(ml, Button::cancel, false);
                } else if (e.type == SDL_JOYAXISMOTION) {
                        if (e.caxis.axis == 0) {
                                bool rt = e.caxis.value > 8000;
                                bool lf = e.caxis.value < -8000;
                                if (rt != button_rt) {
                                        propagate_event(ml, Button::right, rt);
                                        button_rt = rt;
                                }
                                if (lf != button_lf) {
                                        propagate_event(ml, Button::left, lf);
                                        button_lf = lf;
                                }
                        } else if (e.caxis.axis == 1) {
                                bool dn = e.caxis.value > 8000;
                                bool up = e.caxis.value < -8000;
                                if (dn != button_dn) {
                                        propagate_event(ml, Button::down, dn);
                                        button_dn = dn;
                                }
                                if (up != button_up) {
                                        propagate_event(ml, Button::up, up);
                                        button_up = up;
                                }
                        }
                } else if (e.type == SDL_KEYDOWN) {
                        if (e.key.keysym.sym == SDLK_ESCAPE)
                                ml.kill();
                        else if (e.key.keysym.sym == SDLK_RETURN) {
                                try {
                                        ret = world.eval(command_line.buf());
                                } catch (std::exception& ex) {
                                        ret = ex.what();
                                }
                                command_line.accept_line();
                                command_line.new_buffer();
                        } else if (e.key.keysym.sym == SDLK_DELETE)
                                command_line.delete_char();
                        else if (e.key.keysym.sym == SDLK_BACKSPACE)
                                command_line.backward_delete_char();
                        else if (e.key.keysym.sym == SDLK_LEFT)
                                command_line.backward_char();
                        else if (e.key.keysym.sym == SDLK_RIGHT)
                                command_line.forward_char();
                        else if (e.key.keysym.sym == SDLK_UP)
                                command_line.previous_history();
                        else if (e.key.keysym.sym == SDLK_DOWN)
                                command_line.next_history();
                        else if (e.key.keysym.sym == SDLK_HOME)
                                command_line.beginning_of_line();
                        else if (e.key.keysym.sym == SDLK_END)
                                command_line.end_of_line();
                } else if (e.type == SDL_TEXTINPUT) {
                        command_line.insert_str(e.text.text, strlen(e.text.text));
                }
        }

 private:
        World& world;
        Command_line command_line;
        std::string ret;
        Explore explore;
        Menu menu;
        Battle battle;
        Mode mode{Mode::explore};
        Texture font{TEXTURE_DIR "font.gif"};
        int message_sub = 0;
        bool button_rt{false};
        bool button_lf{false};
        bool button_up{false};
        bool button_dn{false};

        void render_messages(Main_loop& ml)
        {
                static Vec3i f_col{80, 48, 0};
                static Vec3i b_col{120, 120, 120};

                if (world.messages.empty())
                        return;
                Vec2f window = ml.window_size();
                Vec2i view{(int)window.x / 32, 4};
                Vec2f diff = Vec2f{window.x / 32, 4} - view;
                Box2f box{diff / -2, view + diff / 2};
                if (mode == Mode::explore)
                        glViewport(0, 64, window.x, 128);
                else
                        glViewport(0, window.y - 192, window.x, 128);
                glLoadIdentity();
                glOrtho(box.min.x, box.max.x, box.max.y, box.min.y, 0, 1);
                Quad_renderer renderer{font};
                glColor3ub(240, 208, 176);
                render_quad(renderer, Box2f{Vec2f{0, 0}, view}, 16, 0);
                glPushMatrix();
                auto& s = world.messages.front();
                auto w = line_width(s);
                glTranslatef(view.x / 2. - w / 4., .5, 0);
                render_shaded_text(renderer, s.substr(0, message_sub), f_col, b_col);
                glPopMatrix();
                if (message_sub < world.messages.front().size())
                        message_sub += 3;
        }
        void render_command_line()
        {
                static Vec3i f_col{255, 255, 255};
                static Vec3i b_col{0, 0, 0};

                {
                        Quad_renderer renderer{font};

                        glViewport(8, 8, 16 * 80, 16);
                        glLoadIdentity();
                        glOrtho(0, 80, 1, 0, 0, 1);
                        render_shaded_text(renderer, command_line.buf(), f_col, b_col);

                        glViewport(8, 24, 16 * 80, 16);
                        glLoadIdentity();
                        glOrtho(0, 80, 1, 0, 0, 1);
                        render_shaded_text(renderer, ret, f_col, b_col);
                }

                if ((SDL_GetTicks() / 400) % 2) {
                        glViewport(8, 8, 16 * 80, 16);
                        glLoadIdentity();
                        glOrtho(0, 80, 1, 0, 0, 1);
                        glScalef(.5, 1, 1);
                        glTranslatef(command_line.cur(), 0, 0);
                        Quad_renderer{}.render(1, 0);
                }

        }

        void propagate_event(Main_loop& ml, Button button, bool state)
        {
                if (button == Button::action && state == true) {
                        world.locked = false;
                        if (!world.messages.empty()) {
                                if (message_sub < world.messages.front().size()) {
                                        message_sub = world.messages.front().size();
                                } else {
                                        world.messages.pop_front();
                                        message_sub = 0;
                                }
                                return;
                        }
                }
                switch (mode) {
                 case Mode::explore:
                        explore.event(ml, button, state);
                        break;
                 case Mode::manage:
                        menu.event(ml, button, state);
                        break;
                 case Mode::battle:
                        menu.event(ml, button, state);
                        break;
                }
        }
        void set_mode(Mode new_mode)
        {
                if (mode != new_mode)
                        world.messages.clear();
                switch (mode) {
                 case Mode::explore:
                        switch (new_mode) {
                         case Mode::explore:
                                return;
                         case Mode::manage:
                                menu.setup();
                                break;
                         case Mode::battle:
                                explore.setdown();
                                menu.setup();
                                battle.setup();
                                break;
                        }
                        break;
                 case Mode::manage:
                        switch (new_mode) {
                         case Mode::explore:
                                menu.setdown();
                                break;
                         case Mode::manage:
                                break;
                         case Mode::battle:
                                explore.setdown();
                                battle.setup();
                                break;
                        }
                        break;
                 case Mode::battle:
                        switch (new_mode) {
                         case Mode::explore:
                                battle.setdown();
                                menu.setdown();
                                explore.setup();
                                break;
                         case Mode::manage:
                                battle.setdown();
                                menu.setdown();
                                explore.setup();
                                break;
                         case Mode::battle:
                                break;
                        }
                        break;
                }
                mode = new_mode;
        }
};

Game::Game(World& world)
        : impl{std::make_unique<Game::Impl>(world)}
{}
Game::~Game() = default;
void Game::render(Main_loop& ml)
{
        impl->render(ml);
}
void Game::update(Main_loop& ml)
{
        impl->update(ml);
}
void Game::event(Main_loop& ml, const SDL_Event &e)
{
        impl->event(ml, e);
}

