/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef _SRC_APPLICATION_H
#define _SRC_APPLICATION_H

#include "main_loop.h"
#include "command_line.h"
#include "world.h"

class Game : public Event_handler {
 public:
        Game(World& world);
        ~Game();
        void render(Main_loop& ml);
        void update(Main_loop& ml);
        void event(Main_loop& ml, const SDL_Event &e);

 private:
        class Impl;
        std::unique_ptr<Impl> impl;
};

#endif          // _SRC_APPLICATION_H
