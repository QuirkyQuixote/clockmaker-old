/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#include "main_loop.h"

#include <assert.h>
#include <iostream>

class Fps_manager : public Noncopyable {
 public:
        explicit Fps_manager(int fps) : fps{fps}
        {
                assert((fps >= 1) && (fps <= 200));
                t0 = SDL_GetTicks();
                t1 = t0;
                frame_count = 0;
        }
        int sync()
        {
                int target_ticks;
                int time_elapsed = 0;
                int ticks;

                frame_count++;

                ticks = SDL_GetTicks();
                time_elapsed = ticks - t1;
                t1 = ticks;
                target_ticks = t0 + (frame_count * 1000) / fps;

                if (ticks <= target_ticks) {
                        SDL_Delay(target_ticks - ticks);
                } else {
                        t0 = t1;
                        frame_count = 0;
                }

                return time_elapsed;
        }

 private:
        int t0;
        int t1;
        int frame_count;
        int fps;
};

Main_loop::Main_loop()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) != 0)
                throw Sdl_error {SDL_GetError()};
        SDL_DisplayMode mode;
        SDL_GetDesktopDisplayMode(0, &mode);
	sdl_window = SDL_CreateWindow("Foo", 0, 0, mode.w, mode.h,
                        SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
	if (sdl_window == NULL)
                throw Sdl_error {SDL_GetError()};
	gl_context = SDL_GL_CreateContext(sdl_window);
	if (gl_context == NULL)
                throw Sdl_error {SDL_GetError()};
        SDL_GL_SetSwapInterval(1);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 3);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) != 0)
                throw Sdl_error {Mix_GetError()};
	Mix_Init(0);

        std::cout << SDL_NumJoysticks() << " joysticks found:" << std::endl;
        for (int i = 0; i < SDL_NumJoysticks(); ++i) {
                if (SDL_IsGameController(i)) {
                        if (SDL_GameController *c = SDL_GameControllerOpen(i))
                                std::cout << SDL_GameControllerName(c) << std::endl;
                        else
                                std::cerr << SDL_GetError() << std::endl;
                } else{
                        if (SDL_Joystick *j = SDL_JoystickOpen(i))
                                std::cout << SDL_JoystickName(j) << std::endl;
                        else
                                std::cerr << SDL_GetError() << std::endl;
                }
        }
}
Main_loop::~Main_loop()
{
        SDL_GL_DeleteContext(gl_context);
        SDL_DestroyWindow(sdl_window);
        SDL_Quit();
}
void Main_loop::run(Event_handler& eh)
{
        Fps_manager fps_mgr {fps};
        SDL_Event e;
        keep_going = true;

        while (keep_going) {
                while (SDL_PollEvent(&e)) {
                        if (e.type == SDL_QUIT)
                                keep_going = false;
                        else
                                eh.event(*this, e);
                }
                eh.update(*this);
                SDL_GL_MakeCurrent(sdl_window, gl_context);
                eh.render(*this);
                SDL_GL_SwapWindow(sdl_window);
                fps_mgr.sync();
        }
}
GLuint Construct_texture::operator()(const boost::filesystem::path& path)
{
        SDL_Surface *raw = IMG_Load(path.c_str());
        if (raw == NULL)
                throw Sdl_error{IMG_GetError()};
        SDL_Surface *image =
                SDL_ConvertSurfaceFormat(raw, SDL_PIXELFORMAT_RGBA32, 0);
        SDL_FreeSurface(raw);
        if (image == NULL)
                throw Sdl_error{SDL_GetError()};
        int w = image->w;
        int h = image->h;
        void *data = image->pixels;
        GLuint name;
        glGenTextures(1, &name);
        glBindTexture(GL_TEXTURE_2D, name);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        SDL_FreeSurface(image);
        return name;
}
void Destroy_texture::operator()(GLuint texture)
{
}
Mix_Chunk* Construct_chunk::operator()(const boost::filesystem::path& path)
{
        auto chunk = Mix_LoadWAV(path.c_str());
        if (!chunk)
                throw Sdl_error{Mix_GetError()};
        return chunk;
}
void Destroy_chunk::operator()(Mix_Chunk* chunk)
{
}
