/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_MAIN_LOOP_H_
#define SRC_MAIN_LOOP_H_

#include <string>
#include <exception>

#include "boost/filesystem.hpp"

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_mixer.h"
#define GL_GLEXT_PROTOTYPES 1
#include "SDL2/SDL_opengl.h"
#include "GL/glext.h"
#undef GL_GLEXT_PROTOTYPES
#include "GL/glu.h"

#include "noncopyable.h"
#include "dict.h"
#include "vec.h"

#define TEXTURE_DIR DATADIR "/textures/"
#define AUDIO_DIR DATADIR "/samples/"
#define LUA_DIR DATADIR "/lua/"

class Sdl_error : public std::exception {
 public:
        explicit Sdl_error(const std::string &what) : _what(what) {}
        const char *what() const noexcept { return _what.data(); }

 private:
        std::string _what;
};

class Opengl_error : public std::exception {
 public:
        explicit Opengl_error(GLenum what) : _what(what) {}
        const char *what() const noexcept
        {
                if (_what == GL_INVALID_ENUM)
                        return "Unacceptable value for enumerated argument";
                if (_what == GL_INVALID_VALUE)
                        return "Numeric argument out of range";
                if (_what == GL_INVALID_OPERATION)
                        return "Operation is not allowed in the current state";
                if (_what == GL_STACK_OVERFLOW)
                        return "This command would cause a stack overflow";
                if (_what == GL_STACK_UNDERFLOW)
                        return "This command would cause a stack underflow";
                if (_what == GL_OUT_OF_MEMORY)
                        return "Not enough memory left to execute the command";
                if (_what == GL_TABLE_TOO_LARGE)
                        return "The specified table exceeds the maximum size";
                return "Non-standard error code";
        }

 private:
        GLenum _what;
};

template<typename T, class Construct, class Destroy> class Resource {
 public:
        explicit Resource(const boost::filesystem::path& path)
        {
                try {
                        impl = factory().refs.at(path);
                } catch (...) { }
                if (!impl) {
                        impl.reset(new Impl{path});
                        factory().refs[path] = impl;
                }
        }
        Resource() = default;
        ~Resource() = default;

        Resource(const Resource& other) = default;
        Resource(Resource&& other) = default;
        Resource& operator=(const Resource& other) = default;
        Resource& operator=(Resource&& other) = default;

        operator T() const { return impl->data; }

        static void gc()
        {
                auto& r = factory().refs;
                r.erase(std::remove_if(r.begin(), r.end(), [](auto& x){
                                        return x.second.use_count() == 1; }),
                                r.end());
        }

 private:
        struct Impl : public Noncopyable {
                Impl(const boost::filesystem::path& path)
                        : data{factory().construct(path)} {}
                ~Impl()
                {
                        factory().destroy(data);
                }
                T data;
        };
        struct Factory : public Noncopyable {
                Dict<boost::filesystem::path, std::shared_ptr<Impl>> refs;
                Construct construct;
                Destroy destroy;
        };
        static Factory& factory()
        {
                static Factory obj;
                return obj;
        };
        std::shared_ptr<Impl> impl;
};

struct Construct_texture {
        GLuint operator()(const boost::filesystem::path& path);
};
struct Destroy_texture {
        void operator()(GLuint texture);
};
struct Construct_chunk {
        Mix_Chunk* operator()(const boost::filesystem::path& path);
};
struct Destroy_chunk {
        void operator()(Mix_Chunk* chunk);
};

typedef Resource<GLuint, Construct_texture, Destroy_texture> Texture;
typedef Resource<Mix_Chunk*, Construct_chunk, Destroy_chunk> Chunk;

class Main_loop;

struct Event_handler {
        virtual ~Event_handler() = default;
        virtual void render(Main_loop& ml) = 0;
        virtual void update(Main_loop& ml) = 0;
        virtual void event(Main_loop& ml, const SDL_Event& e) = 0;
};

class Main_loop : public Noncopyable {
 public:
        Main_loop();
        ~Main_loop();

        void run(Event_handler& eh);
        void kill() { keep_going = false; }
        int get_fps() const { return fps; }
        Vec2i window_size() const {
                Vec2i ret;
                SDL_GetWindowSize(sdl_window, &ret.x, &ret.y);
                return ret;
        }

 private:
        SDL_Window *sdl_window;
        SDL_GLContext gl_context;
        int fps = 30;
        int keep_going;
};

#endif  // SRC_MAIN_LOOP_H_

