
#include "menu.h"

#include <utility>
#include <sstream>

#include "quad_renderer.h"

class Menu::Impl {
 public:
        Impl(World& world) : world{world} {}
        void setup()
        {
                cur = {0, 0};
                cur_v = {0, 0};
                cur_w = {0, 0};
                cur_rest = {0, 0};
                sel = {-1, -1};
                mode = Mode::select;
        }
        void setdown()
        {
        }
        void update(Main_loop& ml)
        {
                if (world.enemy.empty()) {
                        for (auto& ch : world.party)
                                update_character_1(ch);
                } else {
                        for (auto& ch : world.party)
                                update_character_2(ch);
                        for (auto& ch : world.enemy)
                                update_character_2(ch);
                }
                switch (mode) {
                 case Mode::select:
                 case Mode::confirm:
                        update_selection();
                        break;
                 case Mode::target:
                        update_target();
                        break;
                }
        }
        void render(Main_loop& ml)
        {
                Vec2f window = ml.window_size();
                Vec2i view = window / 32;
                Vec2f diff = (window / 32) - view;
                Box2f box{diff / -2, view + diff / 2};
                glViewport(0, 0, window.x, window.y);
                glLoadIdentity();
                glOrtho(box.min.x, box.max.x, box.max.y, box.min.y, 0, 1);
                Quad_renderer renderer{font};
                render_party(view, renderer);
                render_enemy(view, renderer);
        }
        void event(Main_loop& ml, Button button, bool state)
        {
                switch (button) {
                 case Button::action:
                        if (!state)
                                break;
                        if (cur.x < 0) {
                                Mix_PlayChannel(-1, Chunk(AUDIO_DIR "fail.wav"), 0);
                                return;
                        }
                        switch (mode) {
                         case Mode::select:
                                select_item();
                                break;
                         case Mode::confirm:
                                confirm_item();
                                break;
                         case Mode::target:
                                select_target();
                                break;
                        }
                        break;
                 case Button::cancel:
                        if (!state)
                                break;
                        if (cur.x < 0) {
                                Mix_PlayChannel(-1, Chunk(AUDIO_DIR "fail.wav"), 0);
                                return;
                        }
                        switch (mode) {
                         case Mode::select:
                                Mix_PlayChannel(-1, Chunk(AUDIO_DIR "fail.wav"), 0);
                                break;
                         case Mode::confirm:
                                sel = {-1, -1};
                                mode = Mode::select;
                                Mix_PlayChannel(-1, Chunk(AUDIO_DIR "cancel.wav"), 0);
                                break;
                         case Mode::target:
                                cur = sel;
                                mode = world.enemy.empty() ? Mode::confirm : Mode::select;
                                Mix_PlayChannel(-1, Chunk(AUDIO_DIR "cancel.wav"), 0);
                                break;
                        }
                        break;
                 case Button::left:
                        cur_v.x = -(int)state;
                        break;
                 case Button::right:
                        cur_v.x = (int)state;
                        break;
                 case Button::up:
                        cur_v.y = -(int)state;
                        break;
                 case Button::down:
                        cur_v.y = (int)state;
                        break;
                }
        }

 private:
        enum class Mode {
                select,
                confirm,
                target
        };

        void render_party(const Vec2i& view, Quad_renderer& renderer)
        {
                static Vec3i f_col{80, 48, 0};
                static Vec3i b_col{120, 120, 120};

                for (int i = 0; i < world.party.size(); ++i) {
                        auto& ch = world.party[i];
                        int h = (ch.recovery & ~0x7);

                        glPushMatrix();
                        glTranslatef((view.x - world.party.size() * 9) / 2 + 9 * i + 0.5,
                                        view.y - 16 + h / 16.f, 0);
                        glColor3ub(240, 208, 176);
                        render_quad(renderer, {{0, 0}, {8, 17}}, 16, 0);
                        if (sel.x == i) {
                                Box2i box{{1, sel.y + 6}, {7, sel.y + 7}};
                                glColor3ub(248, 164, 192);
                                render_quad(renderer, box, 16, 0);
                        }
                        switch (mode) {
                         case Mode::select:
                         case Mode::confirm:
                                if (cur.x == i) {
                                        Box2i box{{1, cur.y + 6}, {7, cur.y + 7}};
                                        if (sel != cur) {
                                                glColor3ub(252, 160, 68);
                                                render_quad(renderer, box, 16, 0);
                                        }
                                        glColor3ub(228, 92, 16);
                                        render_empty_quad(renderer, box, 16, 0, 1./16.);
                                }
                                break;
                         case Mode::target:
                                if (cur.y == 0 && cur.x == i) {
                                        Box2i box{{1, 1}, {7, 2}};
                                        glColor3ub(252, 160, 68);
                                        render_quad(renderer, box, 16, 0);
                                        glColor3ub(228, 92, 16);
                                        render_empty_quad(renderer, box, 16, 0, 1./16.);
                                }
                                break;
                        }
                        glTranslatef(1, 1, 0);
                        render_shaded_text(renderer, ch.name, f_col, b_col);
                        glTranslatef(0, 1, 0);
                        render_shaded_text(renderer, "------------", f_col, b_col);
                        glTranslatef(0, 1, 0);
                        render_shaded_text(renderer, format_attr(ch.cur.st) + " " +
                                        format_attr(ch.cur.dx) + " " +
                                        format_attr(ch.cur.iq) + " " +
                                        format_attr(ch.cur.ht), f_col, b_col);
                        glTranslatef(0, 1, 0);
                        render_shaded_text(renderer, " \x01  \x02  \x03  \x04", f_col, b_col);
                        glTranslatef(0, 1, 0);
                        render_shaded_text(renderer, "------------", f_col, b_col);

                        for (auto& x : ch.inventory) {
                                glTranslatef(0, 1, 0);
                                render_shaded_text(renderer, x, f_col, b_col);
                        }
                        glPopMatrix();
                        assert(glGetError() == GL_NO_ERROR);
                }
        }
        void render_enemy(const Vec2i& view, Quad_renderer& renderer)
        {
                static Vec3i f_col{80, 48, 0};
                static Vec3i b_col{120, 120, 120};

                for (int i = 0; i < world.enemy.size(); ++i) {
                        auto& ch = world.enemy[i];

                        glPushMatrix();
                        glTranslatef((view.x - world.enemy.size() * 9) / 2 + 9 * i + 0.5, 8, 0);
                        glColor3ub(240, 208, 176);
                        render_quad(renderer, {{0, 0}, {8, 3}}, 16, 0);
                        if (mode == Mode::target) {
                                if (cur.y == 1 && cur.x == i) {
                                        Box2i box{{1, 1}, {7, 2}};
                                        glColor3ub(252, 160, 68);
                                        render_quad(renderer, box, 16, 0);
                                        glColor3ub(228, 92, 16);
                                        render_empty_quad(renderer, box, 16, 0, 1./16.);
                                }
                        }
                        glTranslatef(1, 1, 0);
                        render_shaded_text(renderer, ch.name, f_col, b_col);
                        glPopMatrix();
                        assert(glGetError() == GL_NO_ERROR);
                }
        }

        void update_character_1(Character& ch)
        {
                if (ch.cur.ht < 1)
                        ch.cur.ht = 1;
                ch.recovery = 0;
                ch.flags.knocked_out = false;
                ch.flags.ready = true;
        }
        void update_character_2(Character& ch)
        {
                if (world.messages.empty() && mode != Mode::target && ch.recovery)
                        --ch.recovery;
                ch.flags.knocked_out = ch.cur.ht <= 0;
                ch.flags.ready = !ch.flags.knocked_out && ch.recovery == 0;
        }
        bool party_member_is_ready(const Character& ch)
        {
                return ch.cur.ht > 0 && ch.recovery == 0;
        }
        int find_ready_party_member()
        {
                for (int y = 0; y < world.party.size(); ++y)
                        if (party_member_is_ready(world.party[y]))
                                return y;
                return -1;
        }
        int find_next_ready_party_member(int x)
        {
                int y = x;
                for (;;) {
                        ++y;
                        if (y == world.party.size())
                                y = 0;
                        if (y == x || party_member_is_ready(world.party[y]))
                                return y;
                }
        }
        int find_prev_ready_party_member(int x)
        {
                int y = x;
                for (;;) {
                        if (y == 0)
                                y = world.party.size();
                        --y;
                        if (y == x || party_member_is_ready(world.party[y]))
                                return y;
                }
        }
        void update_selection()
        {
                if (cur.x < 0 || world.party[cur.x].recovery)
                        cur.x = find_ready_party_member();
                if (cur.x < 0)
                        return;
                if (cur_v.x == 0)
                        cur_rest.x = 0;
                if (cur_rest.x > 0) {
                        --cur_rest.x;
                } else if (cur_v.x < 0) {
                        cur.x = find_prev_ready_party_member(cur.x);
                        cur_rest.x = cur_v.x == cur_w.x ? 2 : 10;
                } else if (cur_v.x > 0) {
                        cur.x = find_next_ready_party_member(cur.x);
                        cur_rest.x = cur_v.x == cur_w.x ? 2 : 10;
                }
                if (cur_v.y == 0)
                        cur_rest.y = 0;
                if (cur_rest.y > 0) {
                        --cur_rest.y;
                } else if (cur_v.y < 0 && cur.y > 0) {
                        --cur.y;
                        cur_rest.y = cur_v.y == cur_w.y ? 2 : 10;
                } else if (cur_v.y > 0 && cur.y < 7) {
                        ++cur.y;
                        cur_rest.y = cur_v.y == cur_w.y ? 2 : 10;
                }
                cur_w = cur_v;
        }
        void update_target()
        {
                if (cur_v.x == 0)
                        cur_rest.x = 0;
                if (cur_rest.x > 0) {
                        --cur_rest.x;
                } else if (cur_v.x < 0) {
                        --cur.x;
                        cur_rest.x = cur_v.x == cur_w.x ? 2 : 10;
                } else if (cur_v.x > 0) {
                        ++cur.x;
                        cur_rest.x = cur_v.x == cur_w.x ? 2 : 10;
                }
                if (cur_v.y == 0)
                        cur_rest.y = 0;
                if (cur_rest.y > 0) {
                        --cur_rest.y;
                } else if (cur_v.y != 0) {
                        cur.y = !cur.y;
                        cur_rest.y = cur_v.y == cur_w.y ? 2 : 10;
                }
                if (world.enemy.empty())
                        cur.y = 0;
                if (cur.y == 0)
                        cur.x = cur.x % world.party.size();
                else
                        cur.x = cur.x % world.enemy.size();
                cur_w = cur_v;
        }
        void select_item()
        {
                sel = cur;
                if (world.enemy.empty()) {
                        mode = Mode::confirm;
                } else {
                        cur = {0, 1};
                        mode = Mode::target;
                }
                Mix_PlayChannel(-1, Chunk(AUDIO_DIR "select.wav"), 0);
        }
        void confirm_item()
        {
                if (cur == sel) {
                        cur = {0, 0};
                        mode = Mode::target;
                } else {
                        auto& i1 = world.party[cur.x].inventory;
                        auto& i2 = world.party[sel.x].inventory;
                        std::swap(i1[cur.y], i2[sel.y]);
                        Mix_PlayChannel(-1, Chunk(AUDIO_DIR "swap.wav"), 0);
                        sel = {-1, -1};
                        mode = Mode::select;
                }
        }
        void select_target()
        {
                std::stringstream buf;
                buf << world.party[sel.x].inventory[sel.y] << ".action(" <<
                        "cm.party(" << sel.x << + "), cm." <<
                        (cur.y ? "enemy" : "party") << "(" << cur.x << "))";
                try {
                        world.eval(buf.str());
                } catch (std::exception& ex) {
                        world.messages.push_back(ex.what());
                } catch (...) {
                        world.messages.push_back("unrecognized error");
                }
                for (auto& ch : world.party)
                        if (ch.cur.ht <= 0)
                                world.messages.push_back(ch.name + " is incapacitated!");
                for (auto& ch : world.enemy)
                        if (ch.cur.ht <= 0)
                                world.messages.push_back(ch.name + " is incapacitated!");
                auto& l = world.enemy;
                l.erase(std::remove_if(l.begin(), l.end(),
                                        [](auto& ch){ return ch.cur.ht <= 0; }),
                                l.end());
                cur = sel;
                sel = {-1, -1};
                mode = Mode::select;
        }
        std::string format_attr(int x)
        {
                std::string s{"  "};
                if (x >= 10)
                        s[0] = '0' + x / 10;
                if (x > 0)
                        s[1] = '0' + x % 10;
                return s;
        }


        World& world;
        Vec2i cur;
        Vec2i cur_v;
        Vec2i cur_w;
        Vec2i cur_rest;
        Vec2i sel;
        Mode mode;
        Texture font{TEXTURE_DIR "font.gif"};
};

Menu::Menu(World& world)
        : impl{std::make_unique<Menu::Impl>(world)}
{}
Menu::~Menu() = default;
void Menu::setup()
{
        impl->setup();
}
void Menu::setdown()
{
        impl->setdown();
}
void Menu::update(Main_loop& ml)
{
        impl->update(ml);
}
void Menu::render(Main_loop& ml)
{
        impl->render(ml);
}
void Menu::event(Main_loop& ml, Button button, bool state)
{
        impl->event(ml, button, state);
}
