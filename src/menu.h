
#ifndef SRC_MENU_H_
#define SRC_MENU_H_

#include "main_loop.h"
#include "world.h"

struct Menu : public Noncopyable {
 public:
        Menu(World& world);
        ~Menu();
        void setup();
        void setdown();
        void update(Main_loop& ml);
        void render(Main_loop& ml);
        void event(Main_loop& ml, Button button, bool state);

 private:
        class Impl;
        std::unique_ptr<Impl> impl;
};

#endif          // SRC_MENU_H_
