/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_NONCOPYABLE_H_
#define SRC_NONCOPYABLE_H_

class Noncopyable {
 public:
	Noncopyable() = default;
	~Noncopyable() = default;

	Noncopyable(const Noncopyable &other) = delete;
	Noncopyable(Noncopyable &&other) = delete;
};

#endif  // SRC_NONCOPYABLE_H_
