/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#include "physics.h"

class Space::Impl {
 public:
        explicit Impl(Table<Shape>& shape)
                : shape{shape}
        {
                geom[1] = {{{0.f, 0.f}, {1.f, 1.f}}};
        }
        void step(float dt)
        {
                for (auto b : bodies) {
                        if (b->resting)
                                b->v.y = std::abs(b->v.x);
                        b->v.y += gravity * dt;
                        b->resting = false;
                        if (b->v.x < -terminal_speed)
                                b->v.x = -terminal_speed;
                        else if (b->v.x > terminal_speed)
                                b->v.x = terminal_speed;
                        if (b->v.y < -terminal_speed)
                                b->v.y = -terminal_speed;
                        else if (b->v.y > terminal_speed)
                                b->v.y = terminal_speed;
                }

                for (auto b : bodies) {
                        b->box.min.x = b->p.x - b->size.x;
                        b->box.max.x = b->p.x + b->size.x;
                        b->box.min.y = b->p.y - b->size.y;
                        b->box.max.y = b->p.y + b->size.y;
                        if (b->v.x > 0)
                                move_xpos(*b, dt);
                        else if (b->v.x < 0)
                                move_xneg(*b, dt);
                        b->box.min.x = b->p.x - b->size.x;
                        b->box.max.x = b->p.x + b->size.x;
                        b->box.min.y = b->p.y - b->size.y;
                        b->box.max.y = b->p.y + b->size.y;
                        if (b->v.y > 0)
                                move_ypos(*b, dt);
                        else if (b->v.y < 0)
                                move_yneg(*b, dt);
                        b->box.min.y = b->p.y - b->size.y;
                        b->box.max.y = b->p.y + b->size.y;
                }

                bodies.clear();
        }
        Query query(Vec2f p, Vec2f d)
        {
                Query q;
                if (d.x < 0)
                        query_xneg(p, d, q);
                else if (d.x > 0)
                        query_xpos(p, d, q);
                if (d.y < 0)
                        query_yneg(p, d, q);
                else if (d.y > 0)
                        query_ypos(p, d, q);
                return q;
        }
        void register_body(Body &b) { bodies.push_back(&b); }

 private:
        const float impulse = 0.001;
        const float gravity = 45;
        const float terminal_speed = 50;

        void move_xpos(Body &b, float dt)
        {
                float best;
                Vec2i best_p;
                bool collide;

                std::tie(best, best_p, collide) = test_blocks_xpos(b, dt);
                test_slopes_xpos(b, dt, best);

                if (collide) {
                        b.v.x = 0;
                        b.p.x = best - b.size.x - impulse;
                        if (b.callback)
                                b.callback({b, best_p, Face::lf});
                        return;
                }
                b.p.x += b.v.x * dt;
        }
        void move_xneg(Body &b, float dt)
        {
                float best;
                Vec2i best_p;
                bool collide;

                std::tie(best, best_p, collide) = test_blocks_xneg(b, dt);
                test_slopes_xneg(b, dt, best);

                if (collide) {
                        b.v.x = 0;
                        b.p.x = best + b.size.x + impulse;
                        if (b.callback)
                                b.callback({b, best_p, Face::rt});
                        return;
                }
                b.p.x += b.v.x * dt;
        }
        void move_ypos(Body &b, float dt)
        {
                float best;
                Vec2i best_p;
                bool collide;

                std::tie(best, best_p, collide) = test_blocks_ypos(b, dt);

                if (collide) {
                        b.v.y = 0;
                        b.p.y = best - b.size.y - impulse;
                        if (b.callback)
                                b.callback({b, best_p, Face::up});
                        b.resting = true;
                        return;
                }
                b.p.y += b.v.y * dt;
        }
        void move_yneg(Body &b, float dt)
        {
                float best;
                Vec2i best_p;
                bool collide;

                std::tie(best, best_p, collide) = test_blocks_yneg(b, dt);

                if (collide) {
                        b.v.y = 0;
                        b.p.y = best + b.size.y + impulse;
                        if (b.callback)
                                b.callback({b, best_p, Face::dn});
                        return;
                }
                b.p.y += b.v.y * dt;
        }

        std::tuple<float, Vec2i, bool> test_blocks_xpos(Body& b, float dt)
        {
                Box2f bb{{b.box.max.x, b.box.min.y}, {b.box.max.x + b.v.x * dt, b.box.max.y}};
                bool collide = false;
                float best = bb.max.x;
                Vec2i best_p;

                for (auto p : Box2i{floor(bb.min), ceil(bb.max)}) {
                        if (shape[p] == Shape::full && shape[p - Vec2i{1, 0}] == Shape::empty) {
                                Box2f bb2{p, p + 1};
                                if (overlap(bb, bb2)) {
                                        if (best > bb2.min.x) {
                                                collide = true;
                                                best = bb2.min.x;
                                                best_p = p;
                                        }
                                }
                        }
                }
                return std::make_tuple(best, best_p, collide);
        }
        std::tuple<float, Vec2i, bool> test_blocks_xneg(Body& b, float dt)
        {
                Box2f bb{{b.box.min.x + b.v.x * dt, b.box.min.y}, {b.box.min.x, b.box.max.y}};
                bool collide = false;
                float best = bb.min.x;
                Vec2i best_p;

                for (auto p : Box2i{floor(bb.min), ceil(bb.max)}) {
                        if (shape[p] == Shape::full && shape[p + Vec2i{1, 0}] == Shape::empty) {
                                Box2f bb2{p, p + 1};
                                if (overlap(bb, bb2)) {
                                        if (best < bb2.max.x) {
                                                collide = true;
                                                best = bb2.max.x;
                                                best_p = p;
                                        }
                                }
                        }
                }
                return std::make_tuple(best, best_p, collide);
        }
        std::tuple<float, Vec2i, bool> test_blocks_ypos(Body &b, float dt)
        {
                Box2f bb{{b.box.min.x, b.box.max.y}, {b.box.max.x, b.box.max.y + b.v.y * dt}};
                bool collide = false;
                float best = bb.max.y;
                Vec2i best_p;

                for (auto p : Box2i{floor(bb.min), ceil(bb.max)}) {
                        if (shape[p] == Shape::full) {
                                Box2f bb2{p, p + 1};
                                if (overlap(bb, bb2) && best > bb2.min.y) {
                                        collide = true;
                                        best = bb2.min.y;
                                        best_p = p;
                                }
                        }
                }

                {
                        Vec2f p{b.box.max.x, b.box.max.y};
                        for (Vec2i i{floor(p)}; i.y < best; ++i.y) {
                                if (shape[i] == Shape::slope_lf_1) {
                                        collide = true;
                                        best = i.y + 1 - (p.x - i.x);
                                        best_p = i;
                                        break;
                                }
                        }
                }

                {
                        Vec2f p{b.box.min.x, b.box.max.y};
                        for (Vec2i i{floor(p)}; i.y < best; ++i.y) {
                                if (shape[i] == Shape::slope_rt_1) {
                                        collide = true;
                                        best = i.y + (p.x - i.x);
                                        best_p = i;
                                        break;
                                }
                        }
                }
                return std::make_tuple(best, best_p, collide);
        }
        std::tuple<float, Vec2i, bool> test_blocks_yneg(Body &b, float dt)
        {
                Box2f bb{{b.box.min.x, b.box.min.y + b.v.y * dt}, {b.box.max.x, b.box.min.y}};
                bool collide = false;
                float best = bb.min.y;
                Vec2i best_p;

                for (auto p : Box2i{floor(bb.min), ceil(bb.max)}) {
                        if (shape[p] == Shape::full) {
                                Box2f bb2{p, p + 1};
                                if (overlap(bb, bb2) && best < bb2.max.y) {
                                        collide = true;
                                        best = bb2.max.y;
                                        best_p = p;
                                }
                        }
                }
                return std::make_tuple(best, best_p, collide);
        }

        void test_slopes_xpos(Body& b, float dt, float best)
        {
                bool collide = false;
                Vec2f p{b.box.max.x, b.box.max.y + b.v.y * dt};
                for (Vec2i i{floor(p)}; i.x < best; ++i.x) {
                        if (shape[i] == Shape::slope_lf_1) {
                                if (best - i.x >= 1) {
                                        p.y = i.y - impulse;
                                        --i.y;
                                        collide = true;
                                } else {
                                        float y = i.y + 1 - (best - i.x) - impulse;
                                        if (p.y > y) {
                                                p.y = y;
                                                collide = true;
                                        }
                                }
                        }
                }
                if (collide) {
                        b.p.y = p.y - b.size.y;
                        b.v.y = std::min(b.v.y, 0.f);
                        b.resting = true;
                }
        }
        void test_slopes_xneg(Body& b, float dt, float best)
        {
                bool collide = false;
                Vec2f p{b.box.min.x, b.box.max.y + b.v.y * dt};
                for (Vec2i i{floor(p)}; i.x > best - 1; --i.x) {
                        if (shape[i] == Shape::slope_rt_1) {
                                if (best - i.x >= 1) {
                                        p.y = i.y - impulse;
                                        --i.y;
                                        collide = true;
                                } else {
                                        float y = i.y + (best - i.x) - impulse;
                                        if (p.y > y) {
                                                p.y = y;
                                                collide = true;
                                        }
                                }
                        }
                }
                if (collide) {
                        b.p.y = p.y - b.size.y;
                        b.v.y = std::min(b.v.y, 0.f);
                        b.resting = true;
                }
        }


        void query_xpos(Vec2f p, Vec2f d, Query &q)
        {
                Box2f bb{p, p + d};
                for (auto c : Box2i{floor(bb.min), ceil(bb.max)}) {
                        for (auto bb2 : geom[(uint8_t)shape[c]]) {
                                bb2 += c;
                                float t = (bb2.min.x - p.x) / d.x;
                                if (t < q.best_t) {
                                        float y = p.y + t * d.y;
                                        if (y >= bb2.min.y && y <= bb2.max.y) {
                                                q.face = Face::lf;
                                                q.p = c;
                                                q.q = p + d * t - c;
                                                q.best_t = t;
                                        }
                                }
                        }
                }
        }
        void query_xneg(Vec2f p, Vec2f d, Query &q)
        {
                Box2f bb{p, p + d};
                for (auto c : Box2i{floor(bb.min), ceil(bb.max)}) {
                        for (auto bb2 : geom[(uint8_t)shape[c]]) {
                                bb2 += c;
                                float t = (bb2.max.x - p.x) / d.x;
                                if (t < q.best_t) {
                                        float y = p.y + t * d.y;
                                        if (y >= bb2.min.y && y <= bb2.max.y) {
                                                q.face = Face::rt;
                                                q.p = c;
                                                q.q = p + d * t - c;
                                                q.best_t = t;
                                        }
                                }
                        }
                }
        }
        void query_ypos(Vec2f p, Vec2f d, Query &q)
        {
                Box2f bb{p, p + d};
                for (auto c : Box2i{floor(bb.min), ceil(bb.max)}) {
                        for (auto bb2 : geom[(uint8_t)shape[c]]) {
                                bb2 += c;
                                float t = (bb2.min.y - p.y) / d.y;
                                if (t < q.best_t) {
                                        float x = p.x + t * d.x;
                                        if (x >= bb2.min.x && x <= bb2.max.x) {
                                                q.face = Face::dn;
                                                q.p = c;
                                                q.q = p + d * t - c;
                                                q.best_t = t;
                                        }
                                }
                        }
                }
        }
        void query_yneg(Vec2f p, Vec2f d, Query &q)
        {
                Box2f bb{p, p + d};
                for (auto c : Box2i{floor(bb.min), ceil(bb.max)}) {
                        for (auto bb2 : geom[(uint8_t)shape[c]]) {
                                bb2 += c;
                                float t = (bb2.max.y - p.y) / d.y;
                                if (t < q.best_t) {
                                        float x = p.x + t * d.x;
                                        if (x >= bb2.min.x && x <= bb2.max.x) {
                                                q.face = Face::up;
                                                q.p = c;
                                                q.q = p + d * t - c;
                                                q.best_t = t;
                                        }
                                }
                        }
                }
        }

        Table<Shape> &shape;
        std::vector<Box2f> geom[256];
        std::vector<Body *> bodies;
};

Space::Space(Table<Shape> &shape)
        : impl{std::make_unique<Space::Impl>(shape)}
{}
Space::~Space() = default;
void Space::step(float dt)
{
        impl->step(dt);
}
Query Space::query(Vec2f p, Vec2f d)
{
        return impl->query(p, d);
}
void Space::register_body(Body &b)
{
        impl->register_body(b);
}
