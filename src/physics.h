/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_PHYSICS_H_
#define SRC_PHYSICS_H_

#include <cmath>
#include <functional>
#include <vector>

#include "noncopyable.h"
#include "table.h"
#include "vec.h"
#include "box.h"

enum class Face {
        none, up, dn, lf, rt
};

enum class Shape : uint8_t {
        empty = 0, full = 1, slope_lf_1 = 2, slope_rt_1 = 3
};

struct Query {
	Face face = Face::none;
        Vec2i p;
	Vec2f q;
        float best_t = 1;
};

struct Contact {
        struct Body &body;
        Vec2i p;
        Face face;
};

struct Body : public Noncopyable {
	Vec2f p;
	Vec2f v;
	Vec2f size;
	Box2f box;
        std::function<void(const Contact &)> callback;
        bool resting = false;

        Body(const Vec2f& p, const Vec2f& size)
                : p{p}, size{size}, box{p - size, p + size} {}
        Body(const Box2f& box)
                : p{center(box)}, size{(box.max - box.min) * .5f}, box{box} {}
};

class Space : public Noncopyable {
 public:
	explicit Space(Table<Shape> &shape);
        ~Space();
	void step(float dt);
	Query query(Vec2f p, Vec2f d);
	void register_body(Body &b);

 private:
        class Impl;
        std::unique_ptr<Impl> impl;
};

#endif  // SRC_PHYSICS_H_

