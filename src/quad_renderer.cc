/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#include "quad_renderer.h"

void Quad_renderer::render(int size, int index) const
{
        static Static_data static_data;
        assert(index < size * size);
        if (size == 1)
                static_data.vbuf.draw(GL_TRIANGLES, 0, 6);
        else if (size == 2)
                static_data.vbuf.draw(GL_TRIANGLES, (index + 1) * 6, 6);
        else if (size == 4)
                static_data.vbuf.draw(GL_TRIANGLES, (index + 5) * 6, 6);
        else if (size == 8)
                static_data.vbuf.draw(GL_TRIANGLES, (index + 21) * 6, 6);
        else if (size == 16)
                static_data.vbuf.draw(GL_TRIANGLES, (index + 85) * 6, 6);
}
Quad_renderer::Static_data::Static_data()
{
        std::vector<Vertex> data;
        make_sheet(data, 1);
        make_sheet(data, 2);
        make_sheet(data, 4);
        make_sheet(data, 8);
        make_sheet(data, 16);
        vbuf.update(data);
}
void Quad_renderer::Static_data::make_sheet(std::vector<Vertex>& data, int s)
{
        for (const auto &p : Box2i{{0, 0}, {s, s}}) {
                GLfloat u0 = static_cast<GLfloat>(p.x) / s;
                GLfloat v0 = static_cast<GLfloat>(p.y) / s;
                GLfloat u1 = u0 + 1.0 / s;
                GLfloat v1 = v0 + 1.0 / s;
                data.push_back({0, 0, u0, v0});
                data.push_back({1, 0, u1, v0});
                data.push_back({1, 1, u1, v1});
                data.push_back({0, 0, u0, v0});
                data.push_back({1, 1, u1, v1});
                data.push_back({0, 1, u0, v1});
        }
}

void render_quad(const Quad_renderer& qr, const Box2f& box, int s, int i)
{
        glPushMatrix();
        glTranslatef(box.min.x, box.min.y, 0);
        glScalef(box.max.x - box.min.x, box.max.y - box.min.y, 0);
        qr.render(s, i);
        glPopMatrix();
}
void render_empty_quad(const Quad_renderer& qr, const Box2f& box, int s, int i, float w)
{
        render_quad(qr, Box2f{box.min, {box.min.x + w, box.max.y}}, s, i);
        render_quad(qr, Box2f{box.min, {box.max.x, box.min.y + w}}, s, i);
        render_quad(qr, Box2f{{box.max.x - w, box.min.y}, box.max}, s, i);
        render_quad(qr, Box2f{{box.min.x, box.max.y - w}, box.max}, s, i);
}
void render_text(const Quad_renderer& qr, const std::string& s,
                const Vec3i& color)
{
        glColor3ub(color.x, color.y, color.z);
        Vec2f p{-.25f, 0.f};
        for (unsigned char c : s) {
                if (c == '\n') {
                        p.x = -.25f;
                        p.y += 1;
                } else {
                        glPushMatrix();
                        glTranslatef(p.x, p.y, 0);
                        qr.render(16, c);
                        glPopMatrix();
                        p.x += .5f;
                }
        }
}
void render_shaded_text(const Quad_renderer& qr, const std::string& s,
                const Vec3i& col_f, const Vec3i& col_b)
{
        glPushMatrix();
        glTranslatef(1./32., 1./32., 0);
        render_text(qr, s, col_b);
        glPopMatrix();
        render_text(qr, s, col_f);
}

