/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_QUAD_RENDERER_H_
#define SRC_QUAD_RENDERER_H_

#include <cassert>
#include <string>

#include "vertex_buffer.h"
#include "box.h"

struct Quad_renderer {
 public:
        Quad_renderer()
        {
                glDisable(GL_TEXTURE_2D);
                glDisable(GL_ALPHA_TEST);
        }
        explicit Quad_renderer(const Texture& texture)
        {
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture);
                glEnable(GL_ALPHA_TEST);
                glAlphaFunc(GL_GREATER, 0.5);
        }
        explicit Quad_renderer(const boost::filesystem::path& path)
                : Quad_renderer{Texture{path}} {}
        void render(int size, int index) const;

 private:
        struct Static_data {
                Static_data();
                void make_sheet(std::vector<Vertex>& data, int s);
                Vertex_buffer vbuf;
        };
};

void render_quad(const Quad_renderer& qr, const Box2f& box, int s, int i);
void render_empty_quad(const Quad_renderer& qr, const Box2f& box, int s, int i, float w);
void render_text(const Quad_renderer& qr, const std::string& s,
                const Vec3i& color);
void render_shaded_text(const Quad_renderer& qr, const std::string& s,
                const Vec3i& col_f, const Vec3i& col_b);

#endif          // SRC_QUAD_RENDERER_H_



