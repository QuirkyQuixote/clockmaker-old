/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_SERIALIZE_H_
#define SRC_SERIALIZE_H_

#include "boost/serialization/export.hpp"
#include "boost/serialization/array.hpp"
#include "boost/serialization/vector.hpp"
#include "boost/serialization/string.hpp"
#include "boost/serialization/unique_ptr.hpp"
#include "boost/serialization/utility.hpp"
#include "boost/serialization/split_free.hpp"

#include "world.h"
#include "game.h"

namespace boost {
namespace serialization {

template<class Archive, int S, typename T>
void serialize(Archive& ar, Vec<S, T>& x, const unsigned int version)
{
        ar & x.v;
}
template<class Archive, int S, typename T>
void serialize(Archive& ar, Box<S, T>& x, const unsigned int version)
{
        ar & x.min;
        ar & x.max;
}
template<class Archive, typename T>
void save(Archive& ar, const Table<T>& x, const unsigned int version)
{
        ar << x.dimensions();
        ar << x.base();
}
template<class Archive, typename T>
void load(Archive& ar, Table<T>& x, const unsigned int version)
{
        Vec2i dim;
        ar >> dim;
        x = Table<T>{dim};
        ar >> x.base();
}
template<class Archive, typename T>
void serialize(Archive& ar, Table<T>& x, const unsigned int version)
{
        split_free(ar, x, version);
}
template<class Archive, typename Key, typename T>
void serialize(Archive& ar, Dict<Key, T>& x, const unsigned int version)
{
        ar & x.base();
}
template<class Archive>
void serialize(Archive& ar, Body& x, const unsigned int version)
{
        ar & x.p;
        ar & x.v;
        ar & x.size;
        ar & x.box.min;
        ar & x.box.max;
}
template<class Archive>
void serialize(Archive& ar, Character_attrs& x, const unsigned int version)
{
        ar & x.st;
        ar & x.dx;
        ar & x.iq;
        ar & x.ht;
        ar & x.hp;
        ar & x.will;
        ar & x.per;
        ar & x.fp;
}
template<class Archive>
void serialize(Archive& ar, Character& x, const unsigned int version)
{
        ar & x.name;
        ar & x.cur;
        ar & x.max;
        ar & x.inventory;
}
template<class Archive>
void serialize(Archive& ar, World& x, const unsigned int version)
{
        ar & x.shape;
        ar & x.front;
        ar & x.back;
        ar & x.party;
        ar & x.enemy;
}

};      // namespace serialization
};      // namespace boost

#endif          // SRC_SERIALIZE_H_
