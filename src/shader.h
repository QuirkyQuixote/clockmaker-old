/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_SHADER_H_
#define SRC_SHADER_H_

#include <exception>
#include <string>

#include "main_loop.h"
#include "noncopyable.h"
#include "slurp.h"

class Bad_shader : public std::exception {
 public:
        explicit Bad_shader(const std::string &what) : _what(what) {}
        const char *what() const noexcept { return _what.data(); }

 private:
        std::string _what;
};

class Shader : public Noncopyable {
 public:
        Shader(const std::string &vpath, const std::string &fpath)
        {
                vobj = load_shader(GL_VERTEX_SHADER, vpath);
                fobj = load_shader(GL_FRAGMENT_SHADER, fpath);
                pobj = link_shaders(vobj, fobj);
        }
	~Shader() = default;

	inline void enable()
	{
		glUseProgram(pobj);
	}
	inline void disable()
	{
		glUseProgram(0);
	}
	inline GLint get_uniform_location(const GLchar *name)
	{
		return glGetUniformLocation(pobj, name);
	}

 private:
        static GLuint load_shader(GLuint type, const std::string &path)
        {
                const GLchar *buf;
                GLint len;
                GLchar lbuf[1024];
                GLint llen;

                std::string str = slurp(path);
                GLuint obj = glCreateShader(type);
                buf = str.data();
                len = str.size();
                glShaderSource(obj, 1, &buf, &len);
                glGetShaderInfoLog(obj, sizeof(lbuf), &llen, lbuf);
                if (llen > 0)
                        throw Bad_shader{path + ": " + lbuf};
                return obj;
        }
        static GLuint link_shaders(GLuint vobj, GLuint fobj)
        {
                GLchar lbuf[1024];
                GLint llen;

                GLuint pobj = glCreateProgram();
                glAttachShader(pobj, vobj);
                glAttachShader(pobj, fobj);
                glLinkProgram(pobj);
                glGetProgramInfoLog(pobj, sizeof(lbuf), &llen, lbuf);
                if (llen > 0)
                        throw Bad_shader{lbuf};
                return pobj;
        }

        GLuint vobj;
        GLuint fobj;
        GLuint pobj;
};


#endif  // SRC_SHADER_H_

