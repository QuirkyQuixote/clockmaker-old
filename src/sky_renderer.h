/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_SKY_RENDERER_H_
#define SRC_SKY_RENDERER_H_

#include "quad_renderer.h"

class Sky_renderer : public Noncopyable {
 public:
        void render(const Box2f &box)
        {
                auto p = box.min;
                auto s = box.max - box.min;
                glPushMatrix();
                glTranslatef(p.x, p.y, 0);
                glScalef(s.x, s.y, 1);
                Quad_renderer quad_renderer{tex};
                quad_renderer.render(1, 0);
                glPopMatrix();
        }

 private:
        Texture tex{TEXTURE_DIR "sky.gif"};
};

#endif          // SRC_SKY_RENDERER_H_


