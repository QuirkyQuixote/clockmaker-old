/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_SLURP_H_
#define SRC_SLURP_H_

#include <string>
#include <fstream>
#include <exception>

class Bad_file {
 public:
        explicit Bad_file(const std::string &what) : _what(what) {}
        const char *what() const noexcept { return _what.data(); }

 private:
        std::string _what;
};

static inline std::string slurp(const std::string &path)
{
        std::ifstream is {path, std::ios::binary | std::ios::ate};
        if (!is)
                throw Bad_file{path + ": can't open"};
        auto size = is.tellg();
        std::string str(size, '\0');
        is.seekg(0);
        if (!is.read(&str[0], size))
                throw Bad_file{path + ": can't read"};
        return str;
}

#endif    // SRC_SLURP_H_
