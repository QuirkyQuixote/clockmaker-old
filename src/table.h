/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_TABLE_H_
#define SRC_TABLE_H_

#include <cstdlib>
#include <vector>
#include <memory>

#include "noncopyable.h"
#include "vec.h"
#include "box.h"

template<typename T> class Table {
 public:
        typedef typename std::vector<T> Base;
        typedef typename Base::size_type size_type;
        typedef typename Base::iterator iterator;
        typedef typename Base::const_iterator const_iterator;
        typedef typename Base::reverse_iterator reverse_iterator;
        typedef typename Base::const_reverse_iterator const_reverse_iterator;

        Table() = default;
        Table(Vec2i dim_) : base_(dim_.x * dim_.y), dim_(dim_) {}
        Table(Vec2i dim_, const T& t) : base_(dim_.x * dim_.y, t), dim_(dim_) {}
        Table(Table &&other) = default;
        Table(const Table &other) = default;
        ~Table() = default;
        Table &operator=(Table &&other) = default;
        Table &operator=(const Table &other) = default;

        T &operator[](Vec2i p) { return base_[p.x + p.y * dim_.x]; }
        const T &operator[](Vec2i p) const { return base_[p.x + p.y * dim_.x]; }
        T &at(Vec2i p) { return base_.at(p.x + p.y * dim_.x); }
        const T &at(Vec2i p) const { return base_.at(p.x + p.y * dim_.x); }
        T *data() { return base_.data(); }
        const T *data() const { return base_.data(); }

        iterator begin() { return base_.begin(); }
        const_iterator begin() const { return base_.begin(); }
        const_iterator cbegin() const { return base_.cbegin(); }
        iterator end() { return base_.end(); }
        const_iterator end() const { return base_.end(); }
        const_iterator cend() const { return base_.cend(); }
        iterator rbegin() { return base_.rbegin(); }
        const_iterator rbegin() const { return base_.rbegin(); }
        const_iterator crbegin() const { return base_.crbegin(); }
        iterator rend() { return base_.rend(); }
        const_iterator rend() const { return base_.rend(); }
        const_iterator crend() const { return base_.crend(); }

        size_type size() const { return base_.size(); }
        Vec2i dimensions() const { return dim_; }
        Base& base() { return base_; }
        const Base& base() const { return base_; }

        void swap(Table& other)
        {
                std::swap(base_, other.base_);
                std::swap(dim_, other.dim_);
        }

 private:
        Base base_;
        Vec2i dim_;
};

template<typename T> static inline void swap(Table<T>& a, Table<T>& b)
{
        a.swap(b);
}

#endif          // SRC_TABLE_H_


