
#include "tile_renderer.h"

#include "vertex_buffer.h"

class Tile_renderer::Impl {
 public:
        Impl(const Texture& tex) : tex{tex} {}
        void update(const Table<uint16_t>& tiles, const Box2f &box)
        {
                auto bb {box / shard_size};
                for (const auto& p : Box2i{floor(bb.min), ceil(bb.max)}) {
                        Box2i sub{p, p + Vec2i{1, 1}};
                        sub *= shard_size;
                        update(tiles, sub, shards[p]);
                }
        }
        void render(const Box2f &box)
        {
                auto bb{box / shard_size};
                Box2i visible{floor(bb.min), ceil(bb.max)};
                visible.min.x = std::min(std::max(visible.min.x, 0), 192);
                visible.max.x = std::min(std::max(visible.max.x, 0), 192);
                visible.min.y = std::min(std::max(visible.min.y, 0), 128);
                visible.max.y = std::min(std::max(visible.max.y, 0), 128);
                glAlphaFunc(GL_GREATER, 0.5);
                glEnable(GL_ALPHA_TEST);
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, tex);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                for (const auto &p : visible)
                        shards[p].draw(GL_TRIANGLES);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_ALPHA_TEST);
                glDisable(GL_TEXTURE_2D);
        }

 private:
        void update(const Table<uint16_t>& tiles, const Box2i &box, Vertex_buffer &vbuf)
        {
                std::vector<Vertex> data;
                for (const auto &p : box) {
                        GLfloat x0 = p.x;
                        GLfloat y0 = p.y;
                        GLfloat x1 = x0 + 1;
                        GLfloat y1 = y0 + 1;
                        auto f = tiles[p];
                        GLfloat u0 = (f % 32) / 32.0;
                        GLfloat v0 = (f / 32) / 32.0;
                        GLfloat u1 = u0 + 1.0 / 32.0;
                        GLfloat v1 = v0 + 1.0 / 32.0;
                        data.push_back({x0, y0, u0, v0});
                        data.push_back({x1, y0, u1, v0});
                        data.push_back({x1, y1, u1, v1});
                        data.push_back({x0, y0, u0, v0});
                        data.push_back({x1, y1, u1, v1});
                        data.push_back({x0, y1, u0, v1});
                }
                vbuf.update(data);
        }

        static const int shard_size = 8;
        Table<Vertex_buffer> shards{Vec2i{256, 256}};
        Texture tex;
};

Tile_renderer::Tile_renderer(const Texture& tex)
        : impl{std::make_unique<Tile_renderer::Impl>(tex)}
{}
Tile_renderer::~Tile_renderer() = default;
void Tile_renderer::update(const Table<uint16_t>& tiles, const Box2f& box)
{
        impl->update(tiles, box);
}
void Tile_renderer::render(const Box2f& box)
{
        impl->render(box);
}

