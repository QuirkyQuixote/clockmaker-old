/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_TILE_RENDERER_H_
#define SRC_TILE_RENDERER_H_

#include <cstdlib>

#include "table.h"
#include "main_loop.h"

class Tile_renderer : public Noncopyable {
 public:
        explicit Tile_renderer(const Texture& tex);
        ~Tile_renderer();
        void update(const Table<uint16_t>& tiles, const Box2f& box);
        void render(const Box2f& box);

 private:
        class Impl;
        std::unique_ptr<Impl> impl;
};

#endif          // SRC_TILE_RENDERER_H_
