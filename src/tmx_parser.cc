/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#include "tmx_parser.h"

#include <istream>
#include <ostream>
#include <sstream>
#include <iostream>

#include "pugixml.hpp"

#include "game.h"
#include "slurp.h"

Table<uint16_t> parse_layer(pugi::xml_node root)
{
        auto w = root.attribute("width");
        if (!w)
                throw std::runtime_error{"unspecified root width"};
        auto h = root.attribute("height");
        if (!h)
                throw std::runtime_error{"unspecified root height"};
        auto data = root.child("data");
        if (!data)
                throw std::runtime_error{"unspecified root data"};
        Vec2i dim{ std::stoi(w.value()), std::stoi(h.value())};
        Table<uint16_t> table{dim, 0};
        std::stringstream ss{data.text().get()};
        for (const auto& p : dim) {
                int num;
                char ch;
                ss >> num >> ch;
                table[p] = num - 1;
        }
        return table;
}
void parse_layer(World& w, pugi::xml_node root)
{
        auto name = root.attribute("name");
        if (strcmp(name.value(), "front") == 0) {
                w.front = parse_layer(root);
        } else if (strcmp(name.value(), "back") == 0) {
                w.back = parse_layer(root);
        } else {
                throw std::runtime_error{"unspecified layer name"};
        }
}
void parse_object_properties(std::ostream& buf, pugi::xml_node root)
{
        if (root) {
                for (auto prop = root.child("property"); prop;
                                prop = prop.next_sibling("property")) {
                        auto k = prop.attribute("name");
                        if (!k)
                                throw std::runtime_error{"unspecified property name"};
                        auto v = prop.attribute("value");
                        if (v)
                                buf << k.value() << "=function(self)\n" << v.value() << "\nend,\n";
                        else
                                buf << k.value() << "=function(self)\n" << prop.text().get() << "\nend,\n";
                }
        }
}
void parse_object(std::ostream& buf, pugi::xml_node root)
{
        auto name = root.attribute("name");
        if (!name)
                throw std::runtime_error{"unspecified object name"};
        auto x = root.attribute("x");
        if (!x)
                throw std::runtime_error{"unspecified object position"};
        auto y = root.attribute("y");
        if (!y)
                throw std::runtime_error{"unspecified object position"};
        auto w = root.attribute("width");
        auto h = root.attribute("height");

        Box2f box;
        box.min.x = box.max.x = std::atoi(x.value()) / 8.f;
        box.min.y = box.max.y = std::atoi(y.value()) / 8.f;
        if (w)
                box.max.x += std::atoi(w.value()) / 8.f;
        if (h)
                box.max.y += std::atoi(h.value()) / 8.f;

        buf << "objects." << name.value() << " = {\n" <<
                "box = cm.Box2f(cm.Vec2f(" << box.min.x << "," << box.min.y << ")," <<
                        "cm.Vec2f(" << box.max.x << "," << box.max.y << ")),\n";
        parse_object_properties(buf, root.child("properties"));
        buf << "}\n";
}
void parse_object_group(std::ostream& buf, pugi::xml_node objgr)
{
        for (auto obj = objgr.child("object"); obj;
                        obj = obj.next_sibling("object"))
                parse_object(buf, obj);
}
void parse_map(World& w, pugi::xml_node map)
{
        for (auto layer = map.child("layer"); layer;
                        layer = layer.next_sibling("layer"))
                parse_layer(w, layer);
        {
                std::ofstream buf{"objects.lua", std::ios::out | std::ios::trunc};
                buf << "objects = objects or {}\n";
                for (auto objgr = map.child("objectgroup"); objgr;
                                objgr = objgr.next_sibling("objectgroup"))
                        parse_object_group(buf, objgr);
        }
        w.eval(boost::filesystem::path{"objects.lua"});
}
void parse_tmx(World& w, const boost::filesystem::path& path)
{
        pugi::xml_document doc;
        doc.load_file(path.string().data());
        parse_map(w, doc.child("map"));
}
void load(World& world, const boost::filesystem::path& path)
{
        try {
                parse_tmx(world, path);
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
        }
        world.shape = Table<Shape>{world.front.dimensions(), Shape::empty};
        for (const auto& p : world.front.dimensions()) {
                switch (world.front[p]) {
                 case 192:
                        world.shape[p] = Shape::full;
                        break;
                 case 193:
                 case 257:
                 case 321:
                        world.shape[p] = Shape::slope_rt_1;
                        break;
                 case 194:
                 case 258:
                 case 322:
                        world.shape[p] = Shape::slope_lf_1;
                        break;
                }
        }
        for (const auto& p : world.back.dimensions()) {
                switch (world.back[p]) {
                 case 192:
                        world.shape[p] = Shape::full;
                        break;
                 case 193:
                 case 257:
                 case 321:
                        world.shape[p] = Shape::slope_rt_1;
                        break;
                 case 194:
                 case 258:
                 case 322:
                        world.shape[p] = Shape::slope_lf_1;
                        break;
                }
        }
}


