/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_TMX_H_
#define SRC_TMX_H_

#include "boost/filesystem.hpp"

#include "world.h"

void load(World& world, const boost::filesystem::path& path);

#endif          // SRC_TMX_H_



