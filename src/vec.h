/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_VEC_H_
#define SRC_VEC_H_

#include <cmath>
#include <cassert>
#include <ostream>
#include <type_traits>
#include <initializer_list>

template<int S, typename T> struct Vec {
        Vec()
        {
                for (int i = 0; i < S; ++i)
                        v[i] = 0;
        }
        template<typename U> Vec(const Vec<S, U>& other)
        {
                for (int i = 0; i < S; ++i)
                        v[i] = other.v[i];
        }
        template<typename U> Vec& operator=(const Vec<S, U>& other)
        {
                for (int i = 0; i < S; ++i)
                        v[i] = other.v[i];
                return *this;
        }
        Vec& operator=(T other)
        {
                for (int i = 0; i < S; ++i)
                        v[i] = other;
                return *this;
        }
        T v[S];
};

template<typename T> struct Vec<1, T> {
        Vec() : a{0} {}
        Vec(T a) : a{a} {}
        template<typename U> Vec(const Vec<1, U>& other)
        {
                a = other.a;
        }
        template<typename U> Vec& operator=(const Vec<1, U>& other)
        {
                a = other.a;
                return *this;
        }
        Vec& operator=(T other)
        {
                a = other.a;
                return *this;
        }
        union {
                T v[1];
                struct {
                        union { T x; T a; };
                };
        };
};

template<typename T> struct Vec<2, T> {
        Vec() : a{0}, b{0} {}
        Vec(T a, T b) : a{a}, b{b} {}
        template<typename U> Vec(const Vec<2, U>& other)
        {
                a = other.a;
                b = other.b;
        }
        template<typename U> Vec& operator=(const Vec<2, U>& other)
        {
                a = other.a;
                b = other.b;
                return *this;
        }
        Vec& operator=(T other)
        {
                a = other.a;
                b = other.b;
                return *this;
        }
        union {
                T v[2];
                struct {
                        union { T x; T a; };
                        union { T y; T b; };
                };
        };
};

template<typename T> struct Vec<3, T> {
        Vec() : a{0}, b{0}, c{0} {}
        Vec(T a, T b, T c) : a{a}, b{b}, c{c} {}
        template<typename U> Vec(const Vec<3, U>& other)
        {
                a = other.a;
                b = other.b;
                c = other.c;
        }
        template<typename U> Vec& operator=(const Vec<3, U>& other)
        {
                a = other.a;
                b = other.b;
                c = other.c;
                return *this;
        }
        Vec& operator=(T other)
        {
                a = other.a;
                b = other.b;
                c = other.c;
                return *this;
        }
        union {
                T v[3];
                struct {
                        union { T x; T a; };
                        union { T y; T b; };
                        union { T z; T c; };
                };
        };
};

template<typename T> struct Vec<4, T> {
        Vec() : a{0}, b{0}, c{0}, d{0} {}
        Vec(T a, T b, T c, T d) : a{a}, b{b}, c{c}, d{d} {}
        template<typename U> Vec(const Vec<4, U>& other)
        {
                a = other.a;
                b = other.b;
                c = other.c;
                d = other.d;
        }
        template<typename U> Vec& operator=(const Vec<4, U>& other)
        {
                a = other.a;
                b = other.b;
                c = other.c;
                d = other.d;
                return *this;
        }
        Vec& operator=(T other)
        {
                a = other.a;
                b = other.b;
                c = other.c;
                d = other.d;
                return *this;
        }
        union {
                T v[4];
                struct {
                        union { T x; T a; };
                        union { T y; T b; };
                        union { T z; T c; };
                        union { T w; T d; };
                };
        };
};

template<int S, typename A, typename B> Vec<S, A>& operator+=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] += b.v[i];
        return a;
}
template<int S, typename A, typename B> Vec<S, A>& operator+=(Vec<S, A>& a, B other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] += other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator-=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] -= b.v[i];
        return a;
}
template<int S, typename A, typename B> Vec<S, A>& operator-=(Vec<S, A>& a, B other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] -= other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator*=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] *= b.v[i];
        return a;
}
template<int S, typename A, typename B> Vec<S, A>& operator*=(Vec<S, A>& a, B other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] *= other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator/=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] /= b.v[i];
        return a;
}
template<int S, typename A, typename B> Vec<S, A>& operator/=(Vec<S, A>& a, B other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] /= other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator%=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] %= b.v[i];
        return a;
}
template<int S, typename A> Vec<S, A>& operator%=(Vec<S, A>& a, long other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] %= other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator<<=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] <<= b.v[i];
        return a;
}
template<int S, typename A> Vec<S, A>& operator<<=(Vec<S, A>& a, long other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] <<= other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator>>=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] >>= b.v[i];
        return a;
}
template<int S, typename A> Vec<S, A>& operator>>=(Vec<S, A>& a, long other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] >>= other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator&=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] &= b.v[i];
        return a;
}
template<int S, typename A> Vec<S, A>& operator&=(Vec<S, A>& a, long other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] &= other;
        return a;
}

template<int S, typename A, typename B> Vec<S, A>& operator|=(Vec<S, A>& a, const Vec<S, B>& b)
{
        for (int i = 0; i < S; ++i)
               a.v[i] |= b.v[i];
        return a;
}
template<int S, typename A> Vec<S, A>& operator|=(Vec<S, A>& a, long other)
{
        for (int i = 0; i < S; ++i)
                a.v[i] |= other;
        return a;
}

template<int S, typename A, typename B> bool operator==(const Vec<S, A>& a, const Vec<S, B>& b)
{
        return std::equal(a.v, a.v + S, b.v);
}
template<int S, typename A, typename B> bool operator!=(const Vec<S, A>& a, const Vec<S, B>& b)
{
        return !std::equal(a.v, a.v + S, b.v);
}
template<int S, typename T> Vec<S, T> operator-(Vec<S, T> v)
{
        for (int i = 0; i < S; ++i)
                v[i] = -v[i];
        return v;
}

template<int S, typename A, typename B> auto operator+(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] + b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] + b.v[i];
        return r;
}
template<int S, typename A, typename B> auto operator+(const Vec<S, A>& a, B b)
{
        Vec<S, decltype(a.v[0] + b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] + b;
        return r;
}
template<int S, typename A, typename B> auto operator+(A a, const Vec<S, B>& b)
{
        Vec<S, decltype(a + b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a + b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator-(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] - b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] - b.v[i];
        return r;
}
template<int S, typename A, typename B> auto operator-(const Vec<S, A>& a, B b)
{
        Vec<S, decltype(a.v[0] - b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] - b;
        return r;
}
template<int S, typename A, typename B> auto operator-(A a, const Vec<S, B>& b)
{
        Vec<S, decltype(a - b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a - b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator*(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] * b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] * b.v[i];
        return r;
}
template<int S, typename A, typename B> auto operator*(const Vec<S, A>& a, B b)
{
        Vec<S, decltype(a.v[0] * b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] * b;
        return r;
}
template<int S, typename A, typename B> auto operator*(A a, const Vec<S, B>& b)
{
        Vec<S, decltype(a * b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a * b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator/(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] / b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] / b.v[i];
        return r;
}
template<int S, typename A, typename B> auto operator/(const Vec<S, A>& a, B b)
{
        Vec<S, decltype(a.v[0] / b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] / b;
        return r;
}
template<int S, typename A, typename B> auto operator/(A a, const Vec<S, B>& b)
{
        Vec<S, decltype(a / b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a / b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator%(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] % b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] % b.v[i];
        return r;
}
template<int S, typename A> auto operator%(const Vec<S, A>& a, long b)
{
        Vec<S, decltype(a.v[0] % b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] % b;
        return r;
}
template<int S, typename B> auto operator%(long a, const Vec<S, B>& b)
{
        Vec<S, decltype(a % b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a % b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator<<(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] << b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] << b.v[i];
        return r;
}
template<int S, typename A> auto operator<<(const Vec<S, A>& a, long b)
{
        Vec<S, decltype(a.v[0] << b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] << b;
        return r;
}
template<int S, typename B> auto operator<<(long a, const Vec<S, B>& b)
{
        Vec<S, decltype(a << b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a << b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator>>(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] >> b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] >> b.v[i];
        return r;
}
template<int S, typename A> auto operator>>(const Vec<S, A>& a, long b)
{
        Vec<S, decltype(a.v[0] >> b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] >> b;
        return r;
}
template<int S, typename B> auto operator>>(long a, const Vec<S, B>& b)
{
        Vec<S, decltype(a >> b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a >> b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator&(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] & b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] & b.v[i];
        return r;
}
template<int S, typename A> auto operator&(const Vec<S, A>& a, long b)
{
        Vec<S, decltype(a.v[0] & b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] & b;
        return r;
}
template<int S, typename B> auto operator&(long a, const Vec<S, B>& b)
{
        Vec<S, decltype(a & b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a & b.v[i];
        return r;
}

template<int S, typename A, typename B> auto operator|(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(a.v[0] | b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] | b.v[i];
        return r;
}
template<int S, typename A> auto operator|(const Vec<S, A>& a, long b)
{
        Vec<S, decltype(a.v[0] | b)> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a.v[i] | b;
        return r;
}
template<int S, typename B> auto operator|(long a, const Vec<S, B>& b)
{
        Vec<S, decltype(a | b.v[0])> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = a | b.v[i];
        return r;
}

template<int S, typename A, typename B> auto pow(const Vec<S, A>& a, const Vec<S, B>& b)
{
        Vec<S, decltype(std::pow(a.v[0], b.v[0]))> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = std::pow(a.v[i], b.v[i]);
        return r;
}
template<int S, typename A, typename B> auto pow(const Vec<S, A>& a, B b)
{
        Vec<S, decltype(std::pow(a.v[0], b))> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = std::pow(a.v[i], b);
        return r;
}
template<int S, typename A, typename B> auto pow(A a, const Vec<S, B>& b)
{
        Vec<S, decltype(std::pow(a, b.v[0]))> r;
        for (int i = 0; i < S; ++i)
                r.v[i] = std::pow(a, b.v[i]);
        return r;
}

template<int S, typename T> Vec<S, T> floor(const Vec<S, T>& v)
{
        Vec<S, T> w;
        for (int i = 0; i < S; ++i)
                w.v[i] = std::floor(v.v[i]);
        return w;
}

template<int S, typename T> Vec<S, T> ceil(const Vec<S, T>& v)
{
        Vec<S, T> w;
        for (int i = 0; i < S; ++i)
                w.v[i] = std::ceil(v.v[i]);
        return w;
}

template<int S, typename T> T size(const Vec<S, T>& a)
{
        T r = a.v[0];
        for (int i = 1; i < S; ++i)
                r = std::hypot(r, a.v[i]);
        return r;
}
template<int S, typename T> Vec<S, T> normalize(const Vec<S, T>& a)
{
        return a / size(a);
}
template<int S, typename T> Vec<S, T> normalize_safe(const Vec<S, T>& a)
{
        auto s = size(a);
        return s ? a / s : a;
}
template<int S, typename T> Vec<S, T> flip(const Vec<S, T>& a, int axis)
{
        a.v[axis] = -a.v[axis];
        return a;
}
template<int S, typename T> Vec<S, T> rotate(const Vec<S, T>& v, T a, int axis1, int axis2)
{
	T c = std::cos(a);
	T s = std::sin(a);
        T x = v.v[axis1];
        T y = v.v[axis2];
        v.v[axis1] = c * x - s * y;
        v.v[axis2] = s * x + c * y;
        return v;
}
template<int S, typename T> std::ostream& operator<<(std::ostream& s, const Vec<S, T>& v)
{
        s << '(' << v.v[0];
        for (int i = 1; i < S; ++i)
                s << ' ' << v.v[i];
        return s << ')';
}

template<int S, typename T> struct Vec_iterator {
        const Vec<S, T> &base;
        Vec<S, T> cur;

        Vec_iterator<S, T>& operator++()
        {
                for (int i = 0; i < S; ++i) {
                        ++cur.v[i];
                        if (cur.v[i] != base.v[i] || i == S - 1)
                                break;
                        else
                                cur.v[i] = 0;
                }
                return *this;
        }
        bool operator==(const Vec_iterator<S, T>& other) const
        {
                return cur == other.cur;
        }
        bool operator!=(const Vec_iterator<S, T>& other) const
        {
                return cur != other.cur;
        }
        const Vec<S, T>& operator*() const { return cur; }
        const Vec<S, T>* operator->() const { return &cur; }
};

template<int S, typename T> Vec_iterator<S, T> begin(const Vec<S, T>& v)
{
        return Vec_iterator<S, T>{v, Vec<S, T>{}};
}
template<int S, typename T> Vec_iterator<S, T> end(const Vec<S, T>& v)
{
        Vec<S, T> o;
        o.v[S - 1] = v.v[S - 1];
        return Vec_iterator<S, T>{v, o};
}

typedef Vec<2, int> Vec2i;
typedef Vec<2, float> Vec2f;
typedef Vec<3, int> Vec3i;
typedef Vec<3, float> Vec3f;
typedef Vec<4, int> Vec4i;
typedef Vec<4, float> Vec4f;

#endif          // SRC_VEC_H_
