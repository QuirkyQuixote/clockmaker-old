/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_VERTEX_BUFFER_H_
#define SRC_VERTEX_BUFFER_H_

#include <cassert>
#include <vector>

#include "table.h"
#include "main_loop.h"
#include "noncopyable.h"

struct Vertex {
        GLfloat x, y;
        GLfloat u, v;
};

class Vertex_buffer : public Noncopyable {
 public:
        Vertex_buffer()
        {
                glGenBuffers(1, &name);
                if (GLenum err = glGetError())
                        throw Opengl_error(err);
        }
        ~Vertex_buffer()
        {
                glDeleteBuffers(1, &name);
        }
        void draw(GLenum mode, size_t first, size_t count) const
        {
                assert(first + count <= size);
                assert(glGetError() == GL_NO_ERROR);
                glBindBuffer(GL_ARRAY_BUFFER, name);
                glEnableClientState(GL_VERTEX_ARRAY);
                glEnableClientState(GL_TEXTURE_COORD_ARRAY);
                glVertexPointer(2, GL_FLOAT, sizeof(Vertex),
                                reinterpret_cast<char *>(0));
                glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex),
                                reinterpret_cast<char *>(offsetof(Vertex, u)));
                glDrawArrays(mode, first, count);
                glDisableClientState(GL_VERTEX_ARRAY);
                glDisableClientState(GL_TEXTURE_COORD_ARRAY);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                if (GLenum err = glGetError())
                        throw Opengl_error(err);
        }
        void draw(GLenum mode) const
        {
                draw(mode, 0, size);
        }
        void update(const std::vector<Vertex> &data)
        {
                assert(glGetError() == GL_NO_ERROR);
                glBindBuffer(GL_ARRAY_BUFFER, name);
                glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(Vertex),
                                data.data(), GL_STATIC_DRAW);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                size = data.size();
                if (GLenum err = glGetError())
                        throw Opengl_error(err);
        }

 private:
        GLuint name;
        int size = 0;
};

#endif          // SRC_VERTEX_BUFFER_H_

