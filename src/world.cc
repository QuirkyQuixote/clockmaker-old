/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#include "world.h"

#include <chrono>
#include <thread>
#include <iostream>
#include <sstream>
#include <sstream>
#include <regex>
#include <random>

#include "luabind/luabind.hpp"
#include "luabind/operator.hpp"

#include "game.h"
#include "audio_effects.h"
#include "quad_renderer.h"

namespace luabind {

template<> struct default_converter<boost::filesystem::path>
        : luabind::native_converter_base<boost::filesystem::path>
        {
                static int compute_score(lua_State* L, int index)
                {
                        return lua_type(L, index) == LUA_TSTRING ? 0 : -1;
                }
                boost::filesystem::path from(lua_State* L, int index)
                {
                        return boost::filesystem::path{lua_tostring(L, index)};
                }
                void to(lua_State* L, boost::filesystem::path const& x)
                {
                        lua_pushstring(L, x.string().data());
                }
        };

template<> struct default_converter<boost::filesystem::path const&>
        : default_converter<boost::filesystem::path> {};

template<> struct default_converter<Face>
        : luabind::native_converter_base<Face>
        {
                static int compute_score(lua_State* L, int index)
                {
                        return lua_type(L, index) == LUA_TNUMBER ? 0 : -1;
                }
                Face from(lua_State* L, int index)
                {
                        return (Face)lua_tonumber(L, index);
                }
                void to(lua_State* L, Face x)
                {
                        lua_pushnumber(L, (int)x);
                }
        };

template<> struct default_converter<Face const&>
        : default_converter<Face> {};
}; // namespace luabind 

namespace audio {
struct Source {
        int channel;
        Source(const Chunk& chunk, int repeat)
        {
                channel = Mix_PlayChannel(-1, chunk, repeat);
                world().audio_channels.push_back(channel);
        }
        Source(const boost::filesystem::path& path, int repeat)
                : Source{Chunk{path}, repeat} {}
};

void room_effect(Source& src, const Box2f& box)
{
       ::room_effect(src.channel, box, &world().camera);
}
void mute_effect(Source& src, int enable)
{
        ::mute_effect(src.channel, enable);
}
} // namespace audio

namespace clockmaker_commands {
        void say(const char* line)
        {
                world().messages.push_back(line);
        }
        Character& party(int i)
        {
                return world().party[i];
        }
        Character& enemy(int i)
        {
                return world().enemy[i];
        }
        Character& add_party(const char* name)
        {
                world().party.push_back({name});
                return world().party.back();
        }
        Character& add_enemy(const char* name)
        {
                world().enemy.push_back({name});
                return world().enemy.back();
        }
        int roll(int n)
        {
                static std::random_device rd;
                static std::mt19937 gen(rd());
                static std::uniform_int_distribution<> dis(1, 6);
                int r = 0;
                for (int i = 0; i < n; ++i)
                        r += dis(gen);
                return r;
        }
        int get_mode()
        {
                return (int)world().mode;
        }
        void set_mode(int mode)
        {
                world().mode = (Mode)mode;
        }
        void lock()
        {
                world().locked = true;
        }
        void unlock()
        {
                world().locked = false;
        }
        bool locked()
        {
                return world().locked;
        }
        Space& space()
        {
                return world().space;
        }
        void set_cam(const Vec2f& cam)
        {
                world().camera = cam;
        }
        const Vec2f& get_cam()
        {
                return world().camera;
        }
}; // namespace clockmaker_commands

World::World()
{
        state = luaL_newstate();
        luaL_openlibs(state);

        luabind::open(state);
        luabind::module(state, "cm") [
                luabind::class_<Vec2f>("Vec2f")
                        .def(luabind::constructor<float, float>())
                        .def_readwrite("x", &Vec2f::x)
                        .def_readwrite("y", &Vec2f::y)
                        .def(luabind::tostring(luabind::self)),
                luabind::class_<Box2f>("Box2f")
                        .def(luabind::constructor<Vec2f, Vec2f>())
                        .def_readwrite("min", &Box2f::min)
                        .def_readwrite("max", &Box2f::max)
                        .def(luabind::tostring(luabind::self)),
                luabind::def("center", (Vec2f(*)(const Box2f&))&center),
                luabind::def("overlap", (bool(*)(const Box2f&, const Box2f&))&overlap),
                luabind::def("flip", (Box2f(*)(const Box2f&, int))&flip),
                luabind::def("shift", (Box2f(*)(const Box2f&, const Vec2f&))&shift),
                luabind::def("grow", (Box2f(*)(const Box2f&, float))&grow),
                luabind::def("grow", (Box2f(*)(const Box2f&, const Vec2f&))&grow),
                luabind::def("shrink", (Box2f(*)(const Box2f&, float))&shrink),
                luabind::def("shrink", (Box2f(*)(const Box2f&, const Vec2f&))&shrink),
                luabind::class_<Vec2i>("Vec2i")
                        .def(luabind::constructor<int, int>())
                        .def_readwrite("x", &Vec2i::x)
                        .def_readwrite("y", &Vec2i::y)
                        .def(luabind::tostring(luabind::self)),
                luabind::class_<Box2i>("Box2i")
                        .def(luabind::constructor<Vec2i, Vec2i>())
                        .def_readwrite("min", &Box2i::min)
                        .def_readwrite("max", &Box2i::max)
                        .def(luabind::tostring(luabind::self)),
                luabind::def("center", (Vec2i(*)(const Box2i&))&center),
                luabind::def("overlap", (bool(*)(const Box2i&, const Box2i&))&overlap),
                luabind::def("flip", (Box2i(*)(const Box2i&, int))&flip),
                luabind::def("shift", (Box2i(*)(const Box2i&, const Vec2i&))&shift),
                luabind::def("grow", (Box2i(*)(const Box2i&, int))&grow),
                luabind::def("grow", (Box2i(*)(const Box2i&, const Vec2i&))&grow),
                luabind::def("shrink", (Box2i(*)(const Box2i&, int))&shrink),
                luabind::def("shrink", (Box2i(*)(const Box2i&, const Vec2i&))&shrink),
                luabind::class_<Table<uint16_t>>("Layer")
                        .def("dimensions", &Table<uint16_t>::dimensions),
                luabind::class_<Character_attrs>("Character_attrs")
                        .def_readwrite("st", &Character_attrs::st)
                        .def_readwrite("dx", &Character_attrs::dx)
                        .def_readwrite("iq", &Character_attrs::iq)
                        .def_readwrite("ht", &Character_attrs::ht),
                luabind::class_<Character_flags>("Character_flags")
                        .def_readwrite("ko", &Character_flags::knocked_out)
                        .def_readwrite("ready", &Character_flags::ready),
                luabind::class_<Character>("Character")
                        .def_readwrite("name", &Character::name)
                        .def_readwrite("cur", &Character::cur)
                        .def_readwrite("max", &Character::max)
                        .def_readwrite("flags", &Character::flags)
                        .def("has", &Character::has)
                        .def("give", &Character::give)
                        .def("take", &Character::take)
                        .def("wait", &Character::wait),
                luabind::namespace_("graphics") [
                        luabind::class_<Texture>("Texture")
                                .def(luabind::constructor<const boost::filesystem::path&>()),
                        luabind::class_<Quad_renderer>("Quad_renderer")
                                .def(luabind::constructor<>())
                                .def(luabind::constructor<const Texture&>())
                                .def(luabind::constructor<const boost::filesystem::path&>()),
                        luabind::def("render_quad", &render_quad),
                        luabind::def("render_empty_quad", &render_empty_quad),
                        luabind::def("render_text", &render_text),
                        luabind::def("render_shaded_text", &render_shaded_text)
                ],
                luabind::namespace_("audio") [
                        luabind::class_<Chunk>("Buffer")
                                .def(luabind::constructor<const boost::filesystem::path&>()),
                        luabind::class_<audio::Source>("Source")
                                .def(luabind::constructor<const Chunk&, int>())
                                .def(luabind::constructor<const boost::filesystem::path&, int>()),
                        luabind::def("mute_effect", &audio::mute_effect),
                        luabind::def("room_effect", &audio::room_effect)
                ],
                luabind::namespace_("physics") [
                        luabind::class_<Body>("Body")
                                .def(luabind::constructor<const Box2f&>())
                                .def_readwrite("p", &Body::p)
                                .def_readwrite("v", &Body::v)
                                .def_readonly("size", &Body::size)
                                .def_readonly("box", &Body::box),
                        luabind::class_<Query>("Query")
                                .def_readwrite("p", &Query::p)
                                .def_readwrite("q", &Query::q)
                                .def_readwrite("face", &Query::face),
                        luabind::class_<Space>("Space")
                                .def("step", &Space::step)
                                .def("query", &Space::query)
                                .def("register", &Space::register_body)
                ],
                luabind::def("party", &clockmaker_commands::party),
                luabind::def("enemy", &clockmaker_commands::enemy),
                luabind::def("add_party", &clockmaker_commands::add_party),
                luabind::def("add_enemy", &clockmaker_commands::add_enemy),
                luabind::def("roll", &clockmaker_commands::roll),
                luabind::def("say", &clockmaker_commands::say),
                luabind::def("get_mode", &clockmaker_commands::get_mode),
                luabind::def("set_mode", &clockmaker_commands::set_mode),
                luabind::def("lock", &clockmaker_commands::lock),
                luabind::def("unlock", &clockmaker_commands::unlock),
                luabind::def("locked", &clockmaker_commands::locked),
                luabind::def("space", &clockmaker_commands::space),
                luabind::def("get_cam", &clockmaker_commands::get_cam),
                luabind::def("set_cam", &clockmaker_commands::set_cam)
                ];

        luabind::globals(state)["TEXTURE_DIR"] = TEXTURE_DIR;
        luabind::globals(state)["AUDIO_DIR"] = AUDIO_DIR;
        luabind::globals(state)["cm"]["front"] = front;
        luabind::globals(state)["cm"]["back"] = back;
}
void World::load(const std::string& buf)
{
        if (luaL_loadbuffer(state, buf.data(), buf.size(), buf.data())) {
                std::string ret = lua_tostring(state, -1);
                lua_pop(state, 1);
                throw Lua_error{ret};
        }
}
void World::load(const boost::filesystem::path& path)
{
        if (luaL_loadfile(state, path.string().data())) {
                std::string ret = lua_tostring(state, -1);
                lua_pop(state, 1);
                throw Lua_error{ret};
        }
}
std::string World::call()
{
        if (lua_pcall(state, 0, LUA_MULTRET, 0)) {
                std::string ret = lua_tostring(state, -1);
                lua_pop(state, 1);
                throw Lua_error{ret};
        }
        std::stringstream ret;
        int n = lua_gettop(state);
        while (n) {
                ret << luaL_tolstring(state, -n, NULL);
                if (n > 1)
                        ret << " ";
                --n;
        }
        lua_settop(state, 0);
        return ret.str();
}

World& world()
{
        static World instance;
        return instance;
}

