/* Copyright 2017 Luis Sanz <luis.sanz@gmail.com> */

#ifndef SRC_WORLD_H_
#define SRC_WORLD_H_

#include <memory>
#include <string>
#include <vector>
#include <deque>
#include <iostream>

#include "boost/filesystem.hpp"
#include "lua5.2/lua.hpp"

#include "table.h"
#include "physics.h"
#include "vec.h"
#include "character.h"

class Lua_error : public std::exception {
 public:
        explicit Lua_error(const char* what) : what_{what} {}
        explicit Lua_error(const std::string& what) : what_{what} {}
        const char* what() const noexcept { return what_.data(); }

 private:
        std::string what_;
};

enum class Mode {
        explore,
        manage,
        battle
};

enum class Button { left, right, up, down, action, cancel };

struct World : public Noncopyable {
        Table<uint16_t> front;
        Table<uint16_t> back;
        Table<Shape> shape;
        Space space{shape};
        Vec2f camera;
        std::vector<int> audio_channels;
        std::vector<Character> party = {{ "Levine" }};
        std::vector<Character> enemy;
        std::deque<std::string> messages;
        Mode mode{Mode::explore};
        bool locked{false};
        lua_State* state;

        World();

        std::string eval(const std::string& buf)
        {
                load(buf);
                return call();
        }
        std::string eval(const boost::filesystem::path& path)
        {
                load(path);
                return call();
        }

        void load(const std::string& buf);
        void load(const boost::filesystem::path& path);
        std::string call();
};

World& world();

static inline std::ostream& operator<<(std::ostream& os, Button b)
{
        switch (b) {
         case Button::left:
                return os << "left";
         case Button::right:
                return os << "right";
         case Button::up:
                return os << "up";
         case Button::down:
                return os << "down";
         case Button::action:
                return os << "action";
         case Button::cancel:
                return os << "cancel";
        }
        return os;
}

#endif          // SRC_WORLD_H_
