
#include <iostream>

#include "dict.h"


int main(int argc, char* argv[])
{
        Dict<std::string, std::string> dict;

        while (std::cin) {
                std::string k, v;
                std::cin >> k >> v;
                if (!k.empty())
                        dict[k] = v;
        }
        for (auto& x : dict) {
                std::cout << x.first << " " << x.second << "\n";
        }
        return 0;
}

