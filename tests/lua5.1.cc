
extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

#include <iostream>
#include <string>

int main (void) {
        lua_State *L = lua_open();   /* opens Lua */
        luaL_openlibs(L);
        Wrapper<decltype(cmd_foo), reinterpret_cast<long>(cmd_foo)>::wrap(L);

        while (!std::cin.eof()) {
                std::string line;
                std::cout << ">> ";
                std::getline(std::cin, line);
                if (luaL_loadbuffer(L, line.data(), line.size(), "line") ||
                                lua_pcall(L, 0, 0, 0)) {
                        std::cerr << lua_tostring(L, -1) << "\n";
                        lua_pop(L, 1);  /* pop error message from the stack */
                }
        }

        lua_close(L);
        return 0;
}
