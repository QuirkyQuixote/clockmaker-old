
#include <iostream>
#include <string>

#include "lua.hpp"
#include "luabind/luabind.hpp"

void greet()
{
            std::cout << "hello world!\n";
}

int main (void) {
        lua_State *L = luaL_newstate();   /* opens Lua */
        luaL_openlibs(L);

        luabind::open(L);

        luabind::module(L) [
                luabind::def("greet", &greet)
        ];

        while (!std::cin.eof()) {
                std::string line;
                std::cout << ">> ";
                std::getline(std::cin, line);
                if (luaL_loadbuffer(L, line.data(), line.size(), "line") ||
                                lua_pcall(L, 0, 0, 0)) {
                        std::cerr << lua_tostring(L, -1) << "\n";
                        lua_pop(L, 1);  /* pop error message from the stack */
                }
        }

        lua_close(L);
        return 0;
}

