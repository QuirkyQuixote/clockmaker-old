
#include <unistd.h>
#include <iostream>

#include "tcl_wrap.h"


void cmd_foo()
{
        std::cout << "hello, world\n";
}
const char* cmd_string(const char* val)
{
        return val;
}
char cmd_char(char val)
{
        return val;
}
short cmd_short(short val)
{
        return val;
}
int cmd_int(int val)
{
        return val;
}
long cmd_long(long val)
{
        return val;
}
std::vector<int> cmd_intv(const std::vector<int>& val)
{
        return val;
}
std::array<int, 2> cmd_int2(const std::array<int, 2>& val)
{
        return val;
}
std::array<int, 3> cmd_int3(const std::array<int, 3>& val)
{
        return val;
}
std::tuple<int, int> cmd_intint(const std::tuple<int, int>& val)
{
        return val;
}

int main(int argc, char* argv[])
{
        Tcl_Interp* interp = Tcl_CreateInterp();
        tcl_wrap(interp, "foo", cmd_foo);
        tcl_wrap(interp, "string", cmd_string);
        tcl_wrap(interp, "char", cmd_char);
        tcl_wrap(interp, "short", cmd_short);
        tcl_wrap(interp, "int", cmd_int);
        tcl_wrap(interp, "long", cmd_long);
        tcl_wrap(interp, "intv", cmd_intv);
        tcl_wrap(interp, "int2", cmd_int2);
        tcl_wrap(interp, "int3", cmd_int3);
        tcl_wrap(interp, "intint", cmd_intint);

        while (std::cin) {
                if (isatty(STDIN_FILENO))
                        std::cout << ">> ";
                std::string line;
                std::getline(std::cin, line);
                if (Tcl_Eval(interp, line.data()) != TCL_OK) {
                        std::cout << Tcl_GetStringResult(interp) << "\n";
                } else {
                        const char *str = Tcl_GetStringResult(interp);
                        if (str && *str)
                                std::cout << str << "\n";
                }
        }
        if (isatty(STDIN_FILENO))
                std::cout << "\n";
        return 0;
}

